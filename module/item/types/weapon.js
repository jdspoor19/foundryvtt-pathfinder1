import { ItemPF } from "../entity.js";

export class ItemWeaponPF extends ItemPF {
  async _preUpdate(update, context) {
    // Set weapon subtype if not present
    const newWeaponType = getProperty(update, "data.weaponType");
    if (newWeaponType != null && newWeaponType !== this.data.data.weaponType) {
      const subtype = getProperty(update, "data.weaponSubtype") ?? this.data.data.weaponSubtype ?? "";
      const keys = Object.keys(CONFIG.PF1.weaponTypes[newWeaponType]).filter((o) => !o.startsWith("_"));
      if (!subtype || !keys.includes(subtype)) {
        setProperty(update, "data.weaponSubtype", keys[0]);
      }
    }
  }

  prepareData() {
    const itemData = super.prepareData();
    const data = itemData.data;
    const labels = this.labels;
    const C = CONFIG.PF1;

    // Type and subtype labels
    let wType = this.data.data.weaponType;
    const typeKeys = Object.keys(C.weaponTypes);
    if (!typeKeys.includes(wType)) wType = typeKeys[0];

    let wSubtype = this.data.data.weaponSubtype;
    const subtypeKeys = Object.keys(C.weaponTypes[wType]).filter((o) => !o.startsWith("_"));
    if (!subtypeKeys.includes(wSubtype)) wSubtype = subtypeKeys[0];

    labels.weaponType = C.weaponTypes[wType]._label;
    labels.weaponSubtype = C.weaponTypes[wType][wSubtype];
  }

  /**
   * @param {boolean} active
   * @param {Object} context Optional update context
   * @returns {Promise} Update promise
   * @override
   */
  async setActive(active, context) {
    return this.update({ "data.equipped": active }, context);
  }

  get isActive() {
    return this.data.data.equipped;
  }
}
