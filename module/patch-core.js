import { customRolls } from "./sidebar/chat-message.js";
import { sortArrayByName } from "./lib.js";
import { parseRollStringVariable } from "./roll.js";
import { DicePF } from "./dice.js";

/**
 *
 */
export async function PatchCore() {
  // Patch ChatMessage.applyRollMode to have SELFROLL send to current scene
  const ChatMessage_applyRollMode = ChatMessage.prototype.constructor.applyRollMode;
  const applyRollMode = function (chatData, rollMode) {
    if ( rollMode === "roll" ) rollMode = game.settings.get("core", "rollMode");
    if (rollMode === "gmroll") {
      chatData.whisper = ChatMessage.getWhisperRecipients("GM").map(u => u.id);
    }
    if (rollMode === "blindroll") {
      chatData.whisper = ChatMessage.getWhisperRecipients("GM").map(u => u.id);
      chatData.blind = true;
    }
    if (rollMode === "selfroll") {
      const currentScene = game.user.viewedScene;
      chatData.whisper = game.users.filter((u) => u.viewedScene == currentScene).map((u) => u.id);
    }
    return chatData;
  };
  ChatMessage.prototype.constructor.applyRollMode = applyRollMode;

  // Add inline support for extra /commands
  {
    const origParse = ChatLog.parse;
    ChatLog.parse = function (message) {
      const match = message.match(/^\/(\w+)(?: +([^#]+))(?:#(.+))?/),
        type = match?.[1];
      if (["HEAL", "H", "DAMAGE", "D"].includes(type?.toUpperCase())) {
        match[2] = match[0].slice(1);
        return ["custom", match];
      } else return origParse.call(this, message);
    };

    const origClick = TextEditor._onClickInlineRoll;
    TextEditor._onClickInlineRoll = function (event) {
      event.preventDefault();
      const a = event.currentTarget;
      if (event.ctrlKey) {
        const rollInfo = JSON.parse(decodeURIComponent(event.currentTarget.dataset.roll));
        if (rollInfo.terms[0].class == "Die" && rollInfo.terms[0].faces == 20) {
          // Get the current speaker
          const cls = ChatMessage.implementation;
          const speaker = cls.getSpeaker();
          let actor = cls.getSpeakerActor(speaker);
          let rollData = actor ? actor.getRollData() : {};

          // Obtain roll data from the contained sheet, if the inline roll is within an Actor or Item sheet
          const sheet = a.closest(".sheet");
          if ( sheet ) {
            const app = ui.windows[sheet.dataset.appid];
            if ( ["Actor", "Item"].includes(app?.object?.documentName) ) rollData = app.object.getRollData();
          }

          // Execute a deferred roll
          // const roll = Roll.create(rollInfo.formula, rollData);
          const chatCard = a.closest(".chat-card");
          const chatHeader = chatCard && chatCard.querySelector(".card-header");
          const flavor = chatHeader && chatHeader.textContent.trim();
          // return roll.toMessage({flavor: flavor, speaker});
          const parts = rollInfo.formula.split(' + ').slice(1);
          return DicePF.d20Roll({
            actor: actor,
            event: event,
            parts: parts,
            dice: "1d20",
            data: rollData,
            title: flavor,
            speaker: speaker,
            takeTwenty: false,
            chatTemplate: "systems/pf1/templates/chat/roll-ext.hbs",
            chatMessage: true,
            noSound: false,
            fudge: (event.altKey && event.shiftKey),
          });
        } else {
          // Allow ctrl click to apply damage of damage part
          return CONFIG.Actor.documentClasses.default.applyDamage(rollInfo.total);
        }
      }
      if (!a.classList.contains("custom")) return origClick.call(this, event);

      const chatMessage = `/${a.dataset.formula}`;
      const cMsg = CONFIG.ChatMessage.documentClass;
      const speaker = cMsg.getSpeaker();
      const actor = cMsg.getSpeakerActor(speaker);
      let rollData = actor ? actor.getRollData() : {};

      const sheet = a.closest(".sheet");
      if (sheet) {
        const app = ui.windows[sheet.dataset.appid];
        if (["Actor", "Item"].includes(app?.document.documentName)) rollData = app.object.getRollData();
      }
      return customRolls(chatMessage, speaker, rollData);
    };

    // Fix for race condition
    if ($._data($("body").get(0), "events")?.click?.find((o) => o.selector === "a.inline-roll")) {
      $("body").off("click", "a.inline-roll", origClick);
      $("body").on("click", "a.inline-roll", TextEditor._onClickInlineRoll);
    }
  }

  // Change tooltip showing on alt
  {
    const fn = KeyboardManager.prototype._onAlt;
    KeyboardManager.prototype._onAlt = function (event, up, modifiers) {
      if (!game.pf1.tooltip) return;
      if (!up) game.pf1.tooltip.lock.new = true;
      fn.call(this, event, up, modifiers);
      if (!up) game.pf1.tooltip.lock.new = false;
    };
  }

  // Patch StringTerm
  StringTerm.prototype.evaluate = function (options = {}) {
    const result = parseRollStringVariable(this.term);
    if (typeof result === "string") {
      const src = `with (sandbox) { return ${this.term}; }`;
      try {
        const evalFn = new Function("sandbox", src);
        this._total = evalFn(RollPF.MATH_PROXY);
      } catch (err) {
        err.message = `Failed to evaluate: '${this.term}'\n${err.message}`;
        throw err;
      }
    } else {
      this._total = result;
    }
  };

  // Patch NumericTerm
  NumericTerm.prototype.getTooltipData = function () {
    return {
      formula: this.expression,
      total: this.total,
      flavor: this.flavor,
    };
  };

  // Patch ParentheticalTerm and allowed operators
  ParentheticalTerm.CLOSE_REGEXP = new RegExp(`\\)${RollTerm.FLAVOR_REGEXP_STRING}?`, "g");
  OperatorTerm.REGEXP = /(?:&&|\|\||\*\*|\+|-|\*|\/|\\%|\||:|\?)|(?<![a-z])[!=<>]+/g;
  OperatorTerm.OPERATORS.push("\\%", "!", "?", ":", "=", "<", ">", "==", "===", "<=", ">=", "??", "||", "&&", "**");

  // Add secondary indexing to compendium collections
  {
    const origFunc = CompendiumCollection.prototype.getIndex;
    CompendiumCollection.prototype.getIndex = async function ({ fields } = {}) {
      const index = await origFunc.call(this, { fields });
      this.fuzzyIndex = sortArrayByName([...index]);
      return this.index;
    };
  }

  // Document link attribute stuffing
  {
    const origFunc = TextEditor._createContentLink;
    TextEditor._createContentLink = function (match, type, target, name) {
      const a = origFunc.call(this, match, type, target, name);
      if (name?.indexOf("::") > -1) {
        const args = name.split("::"),
          label = args.pop();
        if (args.length) {
          args.forEach((o) => {
            let [key, value] = o.split(/(?<!\\):/);
            if (!(key && value)) {
              value = key;
              key = "extra";
            }
            switch (key) {
              case "icon":
                a.firstChild.className = "fas fa-" + value;
                break;
              case "class":
                a.classList.add(...value.split(" "));
                break;
              default:
                a.setAttribute("data-" + key, value);
            }
          });
          a.lastChild.textContent = label;
        }
      }
      return a;
    };
  }

  // Remove warnings for conflicting uneditable system bindings
  {
    const origFunc = KeybindingsConfig.prototype._detectConflictingActions;
    KeybindingsConfig.prototype._detectConflictingActions = function (actionId, action, binding) {
      // Uneditable System bindings are never wrong, they can never conflict with something
      if (actionId.startsWith("pf1.") && action.uneditable.includes(binding)) return [];

      return origFunc.call(this, actionId, action, binding);
    };
  }
}
