export class PointBuyCalculator extends DocumentSheet {
  constructor(...args) {
    super(...args);

    const actorAbl = this.actor.data.data.abilities;

    this.abilities = [];
    for (const [k, name] of Object.entries(CONFIG.PF1.abilities)) {
      this.abilities.push({
        key: k,
        name: name,
        value: "",
      });
    }
  }

  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["pf1", "pointbuy-calculator"],
      title: "Point Buy Calculator",
      template: "systems/pf1/templates/apps/point-buy-calculator.hbs",
      width: 320,
      height: "auto",
      closeOnSubmit: false,
      submitOnClose: false,
    });
  }

  get title() {
    return `${this.options.title}: ${this.object.name}`;
  }

  get actor() {
    return this.document;
  }

  getData() {
    const points = this.getSpentPoints();

    return {
      standardArray: Object.keys(CONFIG.PF1.standardArray),
      abilities: this.abilities,
      points: points,
    };
  }

  getSpentPoints() {
    let result = [];
    const array = Object.keys(CONFIG.PF1.standardArray);
    array.forEach((a) => {
      let used = false;
      for (let abl of this.abilities) {
        if (abl.value === parseInt(a)) {
          used = true;
        }
      }
      if (!used) {
        result.push(a);
      }
    });
    return result;
  }

  activateListeners(html) {
    super.activateListeners(html);

    html.find(".ability-control").change(this._onAbilityControl.bind(this));
  }

  _onAbilityControl(event) {
    event.preventDefault();
    const a = event.currentTarget;
    const ablKey = a.closest(".ability").dataset.ability;
    const abl = this.abilities.find((o) => o.key === ablKey);
    abl.value = a.value ? parseInt(a.value) : "";

    this.render();
  }

  _updateObject() {
    const updateData = {};
    for (let a of this.abilities) {
      updateData[`data.abilities.${a.key}.base`] = a.value;
    }
    this.actor.update(updateData);

    this.close();
  }
}
