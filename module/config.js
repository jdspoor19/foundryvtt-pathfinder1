// Namespace PF1 Configuration Values
/**
 * PF1 Configuration Values
 *
 * A dictionary of dictionaries providing configuration data like formulae,
 * translation keys, and other configuration values. Translations keys are
 * assumed to get replaced by their proper translation when the system is loaded.
 *
 * The PF1 object may be adjusted to influence the system's behaviour during runtime.
 *
 * @global
 * @memberof CONFIG
 */
export const PF1 = {
  re: {
    traitSeparator: /\s*[;]\s*/g,
  },

  /**
   * The set of Ability Scores used within the system
   */
  abilities: {
    str: "PF1.AbilityStr",
    dex: "PF1.AbilityDex",
    sta: "PF1.AbilitySta",
    int: "PF1.AbilityInt",
    wis: "PF1.AbilityWis",
    mna: "PF1.AbilityMna",
    prs: "PF1.AbilityPrs",
    cnv: "PF1.AbilityCnv",
    fth: "PF1.AbilityFth",
  },

  /**
   * The set of Ability Scores used within the system in short form
   */
  abilitiesShort: {
    str: "PF1.AbilityShortStr",
    dex: "PF1.AbilityShortDex",
    sta: "PF1.AbilityShortSta",
    int: "PF1.AbilityShortInt",
    wis: "PF1.AbilityShortWis",
    mna: "PF1.AbilityShortMna",
    prs: "PF1.AbilityShortPrs",
    cnv: "PF1.AbilityShortCnv",
    fth: "PF1.AbilityShortFth",
  },

  /**
   * The set of Aspects used within the system
   */
  aspects: {
    bdy: "PF1.AbilityBdy",
    mnd: "PF1.AbilityMnd",
    spr: "PF1.AbilitySpr",
  },

  /**
   * The set of Aspects used within the system in short form
   */
  aspectsShort: {
    bdy: "PF1.AbilityShortBdy",
    mnd: "PF1.AbilityShortMnd",
    spr: "PF1.AbilityShortSpr",
  },

  /**
   * An array containing the HD sizes
   */
  hitDice: ["d4", "d6", "d8", "d10", "d12"],

  /**
   * The standard array used for stat allocation
   */
  standardArray: {
    7: "7",
    9: "9",
    10: "10",
    12: "12",
    13: "13",
    14: "14",
    15: "15",
    16: "16",
    18: "18",
  },

  /**
   * The set of Saving Throws
   */
  savingThrows: {
    bdySave: "PF1.SavingThrowBdy",
    mndSave: "PF1.SavingThrowMnd",
    sprSave: "PF1.SavingThrowSpr",
  },

  /**
   * The set of Saving Throws Progressions
   */
  savingThrowsProg: {
    poor: "PF1.SavingThrowProgPoor",
    medium: "PF1.SavingThrowProgMedium",
    good: "PF1.SavingThrowProgGood",
  },

  /**
   * The set of Armor Classes
   */
  ac: {
    normal: "PF1.ACNormal",
    touch: "PF1.ACTouch",
    flatFooted: "PF1.ACFlatFooted",
  },

  /**
   * The set of Armor Class modifier types
   */
  acValueLabels: {
    normal: "PF1.ACTypeNormal",
    touch: "PF1.ACTypeTouch",
    flatFooted: "PF1.ACTypeFlatFooted",
  },

  /* -------------------------------------------- */

  /**
   * Character alignment options
   */
  alignments: {
    lg: "PF1.AlignmentLG",
    ng: "PF1.AlignmentNG",
    cg: "PF1.AlignmentCG",
    ln: "PF1.AlignmentLN",
    tn: "PF1.AlignmentTN",
    cn: "PF1.AlignmentCN",
    le: "PF1.AlignmentLE",
    ne: "PF1.AlignmentNE",
    ce: "PF1.AlignmentCE",
  },

  /**
   * Character alignment options in their short form
   */
  alignmentsShort: {
    lg: "PF1.AlignmentShortLG",
    ng: "PF1.AlignmentShortNG",
    cg: "PF1.AlignmentShortCG",
    ln: "PF1.AlignmentShortLN",
    tn: "PF1.AlignmentShortTN",
    cn: "PF1.AlignmentShortCN",
    le: "PF1.AlignmentShortLE",
    ne: "PF1.AlignmentShortNE",
    ce: "PF1.AlignmentShortCE",
  },

  /* -------------------------------------------- */

  /**
   * The set of Armor Proficiencies which a character may have
   */
  armorProficiencies: {
    lgt: "PF1.ArmorProfLight",
    med: "PF1.ArmorProfMedium",
    hvy: "PF1.ArmorProfHeavy",
    shl: "PF1.ArmorProfShield",
    twr: "PF1.ArmorProfTowerShield",
  },

  /**
   * The set of broad Weapon Proficiencies a character may have
   */
  weaponProficiencies: {
    meleeProf: "PF1.MeleeWeaponProf",
    meleeExp: "PF1.MeleeWeaponExp",
    rangeProf: "PF1.RangeWeaponProf",
    rangeExp: "PF1.RangeWeaponExp",
  },

  /* -------------------------------------------- */

  /**
   * This describes the ways that an ability can be activated.
   */
  abilityActivationTypes: {
    passive: "PF1.ActivationTypePassive",
    free: "PF1.ActivationTypeFree",
    nonaction: "PF1.ActivationTypeNonaction",
    swift: "PF1.ActivationTypeSwift",
    immediate: "PF1.ActivationTypeImmediate",
    move: "PF1.ActivationTypeMove",
    standard: "PF1.ActivationTypeStandard",
    full: "PF1.ActivationTypeFullround",
    attack: "PF1.ActivationTypeAttack",
    aoo: "PF1.ActivationTypeAoO",
    round: "PF1.ActivationTypeRound",
    minute: "PF1.ActivationTypeMinute",
    hour: "PF1.ActivationTypeHour",
    special: "PF1.ActivationTypeSpecial",
  },

  /**
   * This describes plurals for activation types.
   */
  abilityActivationTypesPlurals: {
    free: "PF1.ActivationTypeFreePlural",
    swift: "PF1.ActivationTypeSwiftPlural",
    immediate: "PF1.ActivationTypeImmediatePlural",
    move: "PF1.ActivationTypeMovePlural",
    standard: "PF1.ActivationTypeStandardPlural",
    full: "PF1.ActivationTypeFullroundPlural",
    attack: "PF1.ActivationTypeAttackPlural",
    round: "PF1.ActivationTypeRoundPlural",
    minute: "PF1.ActivationTypeMinutePlural",
    hour: "PF1.ActivationTypeHourPlural",
  },

  /**
   * This describes the ways that an ability can be activated when using
   * Unchained rules.
   */
  abilityActivationTypes_unchained: {
    passive: "PF1.ActivationTypePassive",
    free: "PF1.ActivationTypeFree",
    nonaction: "PF1.ActivationTypeNonaction",
    reaction: "PF1.ActivationTypeReaction",
    action: "PF1.ActivationTypeAction",
    attack: "PF1.ActivationTypeAttack",
    aoo: "PF1.ActivationTypeAoO",
    minute: "PF1.ActivationTypeMinute",
    hour: "PF1.ActivationTypeHour",
    special: "PF1.ActivationTypeSpecial",
  },

  /**
   * This describes plurals for the ways that an ability can be activated when
   * using Unchained rules.
   */
  abilityActivationTypesPlurals_unchained: {
    passive: "PF1.ActivationTypePassive",
    free: "PF1.ActivationTypeFreePlural",
    reaction: "PF1.ActivationTypeReactionPlural",
    action: "PF1.ActivationTypeActionPlural",
    minute: "PF1.ActivationTypeMinutePlural",
    hour: "PF1.ActivationTypeHourPlural",
    special: "PF1.ActivationTypeSpecial",
  },

  /**
   * The possible conditions when using Wound Threshold rules
   */
  woundThresholdConditions: {
    0: "PF1.WoundLevelHealthy",
    1: "PF1.WoundLevelGrazed",
    2: "PF1.WoundLevelWounded",
    3: "PF1.WoundLevelCritical",
  },

  /**
   * Change targets affected by Wound Thresholds.
   */
  woundThresholdChangeTargets: ["~attackCore", "cmb", "cmd", "allSavingThrows", "ac", "~skillMods", "allChecks"],

  divineFocus: {
    0: "",
    1: "PF1.SpellComponentDivineFocusAlone",
    2: "PF1.SpellComponentDivineFocusMaterial",
    3: "PF1.SpellComponentDivineFocusFocus",
  },

  /**
   * The measure template types available e.g. for spells
   */
  measureTemplateTypes: {
    cone: "PF1.MeasureTemplateCone",
    circle: "PF1.MeasureTemplateCircle",
    ray: "PF1.MeasureTemplateLine",
    rect: "PF1.MeasureTemplateSquare",
  },

  /* -------------------------------------------- */

  /**
   * The possible creature sizes
   */
  actorSizes: {
    fine: "PF1.ActorSizeFine",
    dim: "PF1.ActorSizeDiminutive",
    tiny: "PF1.ActorSizeTiny",
    sm: "PF1.ActorSizeSmall",
    med: "PF1.ActorSizeMedium",
    lg: "PF1.ActorSizeLarge",
    huge: "PF1.ActorSizeHuge",
    grg: "PF1.ActorSizeGargantuan",
    col: "PF1.ActorSizeColossal",
  },

  /**
   * The possible creature sizes in their one-letter form
   */
  sizeChart: {
    fine: "F",
    dim: "D",
    tiny: "T",
    sm: "S",
    med: "M",
    lg: "L",
    huge: "H",
    grg: "G",
    col: "C",
  },

  /**
   * The size values for Tokens according to the creature's size
   */
  tokenSizes: {
    fine: { w: 1, h: 1, scale: 0.45 },
    dim: { w: 1, h: 1, scale: 0.6 },
    tiny: { w: 1, h: 1, scale: 0.75 },
    sm: { w: 1, h: 1, scale: 0.9 },
    med: { w: 1, h: 1, scale: 1 },
    lg: { w: 2, h: 2, scale: 1 },
    huge: { w: 3, h: 3, scale: 1 },
    grg: { w: 4, h: 4, scale: 1 },
    col: { w: 6, h: 6, scale: 1 },
  },

  /**
   * The size modifier applied to creatures not of medium size
   */
  sizeMods: {
    fine: 8,
    dim: 4,
    tiny: 2,
    sm: 1,
    med: 0,
    lg: -1,
    huge: -2,
    grg: -4,
    col: -8,
  },

  /**
   * The size modifier applied to creatures not of medium size
   */
  sizeSpecialMods: {
    fine: -8,
    dim: -4,
    tiny: -2,
    sm: -1,
    med: 0,
    lg: 1,
    huge: 2,
    grg: 4,
    col: 8,
  },

  /**
   * The carry modifier applied to creatures of long stature
   */
  sizeSpecialModsLong: {
    fine: -4,
    dim: -2,
    tiny: -1,
    sm: 0,
    med: 1,
    lg: 2,
    huge: 4,
    grg: 8,
    col: 16,
  },

  /**
   * The size modifier applied to fly checks of creatures not of medium size
   */
  sizeFlyMods: {
    fine: 8,
    dim: 6,
    tiny: 4,
    sm: 2,
    med: 0,
    lg: -2,
    huge: -4,
    grg: -6,
    col: -8,
  },

  /**
   * The size modifier applied to stealth checks of creatures not of medium size
   */
  sizeStealthMods: {
    fine: 16,
    dim: 12,
    tiny: 8,
    sm: 4,
    med: 0,
    lg: -4,
    huge: -8,
    grg: -12,
    col: -16,
  },

  /**
   * The labels used when determining vision using perception total
   */
  visionLabels: {
    0: "PF1.VisionNormal",
    1: "PF1.VisionLowlight",
    2: "PF1.VisionDark30",
    3: "PF1.VisionDark60",
    4: "PF1.VisionDark120",
  },

  /**
   * The dim sight distance for respective perception totals
   */
  visionDimDistance: {
    0: 5,
    1: 5,
    2: 30,
    3: 60,
    4: 120,
  },

  /**
   * The possible options for a creature's maneuverability
   */
  flyManeuverabilities: {
    clumsy: "PF1.FlyManeuverabilityClumsy",
    poor: "PF1.FlyManeuverabilityPoor",
    average: "PF1.FlyManeuverabilityAverage",
    good: "PF1.FlyManeuverabilityGood",
    perfect: "PF1.FlyManeuverabilityPerfect",
  },

  /**
   * The bonus values for a creature's maneuverability
   */
  flyManeuverabilityValues: {
    clumsy: -8,
    poor: -4,
    average: 0,
    good: 4,
    perfect: 8,
  },

  /**
   * The resulting speed values when a base speed is reduced
   */
  speedReduction: {
    5: 5,
    15: 10,
    20: 15,
    30: 20,
    35: 25,
    45: 30,
    50: 35,
    60: 40,
    65: 45,
    75: 50,
    80: 55,
    90: 60,
    95: 65,
    105: 70,
    110: 75,
    120: 80,
  },

  /* -------------------------------------------- */

  /**
   * An array of maximum carry capacities, where the index is the ability/strength score.
   */
  encumbranceLoads: [
    0,
    10,
    20,
    30,
    40,
    50,
    60,
    70,
    80,
    90,
    100,
    115,
    130,
    150,
    175,
    200,
    230,
    260,
    300,
    350,
    400,
    460,
    520,
    600,
    700,
    800,
    920,
    1040,
    1200,
    1400,
    1600,
  ],

  /**
   * Encumbrance multipliers applied due to a creature's size for bi- and
   * quadrupedal creatures.
   */
  encumbranceMultipliers: {
    normal: {
      fine: 0.125,
      dim: 0.25,
      tiny: 0.5,
      sm: 0.75,
      med: 1,
      lg: 2,
      huge: 4,
      grg: 8,
      col: 16,
    },
    quadruped: {
      fine: 0.25,
      dim: 0.5,
      tiny: 0.75,
      sm: 1,
      med: 1.5,
      lg: 3,
      huge: 6,
      grg: 12,
      col: 24,
    },
  },

  /* -------------------------------------------- */

  /**
   * The types for Items
   */
  itemTypes: {
    equipment: "PF1.ItemTypeEquipment",
    weapon: "PF1.ItemTypeWeapon",
    loot: "PF1.ItemTypeLoot",
    consumable: "PF1.ItemTypeConsumable",
    class: "PF1.ItemTypeClass",
    buff: "PF1.ItemTypeBuff",
    spell: "PF1.ItemTypeSpell",
    feat: "PF1.ItemTypeFeat",
    attack: "PF1.ItemTypeAttack",
  },

  /**
   * Classification types for item action types
   */
  itemActionTypes: {
    mwak: "PF1.ActionMWAK",
    rwak: "PF1.ActionRWAK",
    mcman: "PF1.ActionMCMan",
    rcman: "PF1.ActionRCMan",
    ray: "PF1.ActionRay",
    burst: "PF1.ActionBurst",
    bursteffect: "PF1.ActionBurstEffect",
    spellsave: "PF1.ActionSpellSave",
    save: "PF1.ActionSave",
    heal: "PF1.ActionHeal",
    restore: "PF1.ActionRestore",
    other: "PF1.ActionOther",
  },

  /* -------------------------------------------- */

  itemCapacityTypes: {
    items: "PF1.ItemContainerCapacityItems",
    weight: "PF1.ItemContainerCapacityWeight",
  },

  /* -------------------------------------------- */

  /**
   * Enumerate the lengths of time over which an item can have limited use ability
   */
  limitedUsePeriods: {
    resolve: "PF1.LimitedUseResolve",
    single: "PF1.LimitedUseSingle",
    unlimited: "PF1.Unlimited",
    charges: "PF1.LimitedUseCharges",
    shortRest: "PF1.LimitedUseShortRest",
    longRest: "PF1.LimitedUseLongRest",
    safeRest: "PF1.LimitedUseSafeRest",
  },

  /**
   * Rest types
   */
  restPeriods: {
    shortRest: "PF1.ShortRest",
    longRest: "PF1.LongRest",
    safeRest: "PF1.SafeRest",
  },

  /* -------------------------------------------- */

  /**
   * The various equipment types and their subtypes
   */
  equipmentTypes: {
    armor: {
      _label: "PF1.EquipTypeArmor",
      lightArmor: "PF1.EquipTypeLight",
      mediumArmor: "PF1.EquipTypeMedium",
      heavyArmor: "PF1.EquipTypeHeavy",
    },
    shield: {
      _label: "PF1.EquipTypeShield",
      shield: "PF1.EquipTypeShield",
      towerShield: "PF1.EquipTypeTowerShield",
    },
    misc: {
      _label: "PF1.Misc",
      wondrous: "PF1.EquipTypeWondrousItem",
      clothing: "PF1.EquipTypeClothing",
      other: "PF1.Other",
    },
  },

  /**
   * The slots equipment can occupy, sorted by category
   */
  equipmentSlots: {
    armor: {
      armor: "PF1.EquipSlotArmor",
    },
    shield: {
      shield: "PF1.EquipSlotShield",
    },
    misc: {
      slotless: "PF1.EquipSlotSlotless",
      head: "PF1.EquipSlotHead",
      headband: "PF1.EquipSlotHeadband",
      eyes: "PF1.EquipSlotEyes",
      shoulders: "PF1.EquipSlotShoulders",
      neck: "PF1.EquipSlotNeck",
      chest: "PF1.EquipSlotChest",
      body: "PF1.EquipSlotBody",
      belt: "PF1.EquipSlotBelt",
      wrists: "PF1.EquipSlotWrists",
      hands: "PF1.EquipSlotHands",
      ring: "PF1.EquipSlotRing",
      feet: "PF1.EquipSlotFeet",
    },
  },

  /**
   * The subtypes for loot items
   */
  lootTypes: {
    gear: "PF1.LootTypeGear",
    ammo: "PF1.LootTypeAmmo",
    tradeGoods: "PF1.LootTypeTradeGoods",
    misc: "PF1.Misc",
  },

  /**
   * The subtypes for ammo type loot items
   */
  ammoTypes: {
    arrow: "PF1.AmmoTypeArrow",
    bolt: "PF1.AmmoTypeBolt",
    // repeatingBolt: "PF1.AmmoTypeRepeatingBolt",
    slingBullet: "PF1.AmmoTypeBulletSling",
    // gunBullet: "PF1.AmmoTypeBulletGun",
    // dragoonBullet: "PF1.AmmoTypeBulletDragoon",
    dart: "PF1.AmmoTypeDart",
  },

  /* -------------------------------------------- */

  /**
   * Enumerate the valid consumable types which are recognized by the system
   */
  consumableTypes: {
    potion: "PF1.ConsumableTypePotion",
    poison: "PF1.ConsumableTypePoison",
    drug: "PF1.ConsumableTypeDrug",
    scroll: "PF1.ConsumableTypeScroll",
    wand: "PF1.ConsumableTypeWand",
    staff: "PF1.ConsumableTypeStaff",
    misc: "PF1.Misc",
  },

  attackTypes: {
    weapon: "PF1.AttackTypeWeapon",
    natural: "PF1.AttackTypeNatural",
    ability: "PF1.AttackTypeAbility",
    racialAbility: "PF1.AttackTypeRacial",
    item: "PF1.Item",
    misc: "PF1.Misc",
  },

  featTypes: {
    feat: "PF1.FeatTypeFeat",
    trait: "PF1.FeatTypeTraits",
    racial: "PF1.FeatTypeRacial",
    misc: "PF1.Misc",
    template: "PF1.FeatTypeTemplate",
  },

  featTypesPlurals: {
    feat: "PF1.FeatPlural",
    classFeat: "PF1.ClassFeaturePlural",
    trait: "PF1.TraitPlural",
    racial: "PF1.RacialTraitPlural",
    template: "PF1.TemplatePlural",
  },

  /**
   * Feat tiers
   */
   featTiers: {
    0: "PF1.FeatTier0",
    1: "PF1.FeatTier1",
    2: "PF1.FeatTier2",
    3: "PF1.FeatTier3",
    4: "PF1.FeatTier4",
    5: "PF1.FeatTier5",
    6: "PF1.FeatTier6",
    7: "PF1.FeatTier7",
    8: "PF1.FeatTier8",
    9: "PF1.FeatTier9",
  },

  /**
   * Ability types, each with their short and their long form
   */
  abilityTypes: {
    ex: {
      short: "PF1.AbilityTypeShortExtraordinary",
      long: "PF1.AbilityTypeExtraordinary",
    },
    su: {
      short: "PF1.AbilityTypeShortSupernatural",
      long: "PF1.AbilityTypeSupernatural",
    },
    sp: {
      short: "PF1.AbilityTypeShortSpell-Like",
      long: "PF1.AbilityTypeSpell-Like",
    },
  },

  /* -------------------------------------------- */

  /**
   * The valid currency denominations supported by the game system
   */
  currencies: {
    pp: "PF1.CurrencyPP",
    gp: "PF1.CurrencyGP",
    sp: "PF1.CurrencySP",
    cp: "PF1.CurrencyCP",
  },

  acTypes: {
    armor: "Armor",
    shield: "Shield",
    natural: "Natural Armor",
  },

  /**
   * The types of bonus modifiers
   */
  bonusModifiers: {
    untyped: "PF1.BonusModifierUntyped",
    untypedPerm: "PF1.BonusModifierUntypedPerm",
    base: "PF1.BonusModifierBase",
    temp: "PF1.BonusModifierTemp",
    enh: "PF1.BonusModifierEnhancement",
    dodge: "PF1.BonusModifierDodge",
    inherent: "PF1.BonusModifierInherent",
    deflection: "PF1.BonusModifierDeflection",
    morale: "PF1.BonusModifierMorale",
    luck: "PF1.BonusModifierLuck",
    sacred: "PF1.BonusModifierSacred",
    insight: "PF1.BonusModifierInsight",
    resist: "PF1.BonusModifierResistance",
    profane: "PF1.BonusModifierProfane",
    trait: "PF1.BonusModifierTrait",
    racial: "PF1.BonusModifierRacial",
    size: "PF1.BonusModifierSize",
    competence: "PF1.BonusModifierCompetence",
    circumstance: "PF1.BonusModifierCircumstance",
    alchemical: "PF1.BonusModifierAlchemical",
    penalty: "PF1.BonusModifierPenalty",
  },

  /**
   * An array of stacking bonus modifiers by their keys for {@link bonusModifiers}
   */
  stackingBonusModifiers: ["untyped", "untypedPerm", "base", "dodge", "racial", "penalty"],

  /* -------------------------------------------- */

  /**
   * The damage types
   */
  damageTypes: {
    bludgeoning: "PF1.DamageTypeBludgeoning",
    piercing: "PF1.DamageTypePiercing",
    slashing: "PF1.DamageTypeSlashing",
    cold: "PF1.DamageTypeCold",
    fire: "PF1.DamageTypeFire",
    electric: "PF1.DamageTypeElectricity",
    acid: "PF1.DamageTypeAcid",
    force: "PF1.DamageTypeForce",
    radiant: "PF1.DamageTypeRadiant",
    necrotic: "PF1.DamageTypeNecrotic",
  },

  /* -------------------------------------------- */

  /**
   * Valid options for the range of abilities and spells
   */
  distanceUnits: {
    none: "PF1.None",
    personal: "PF1.DistPersonal",
    melee: "PF1.DistMelee",
    reach: "PF1.DistReach",
    close: "PF1.DistClose",
    short: "PF1.DistShort",
    medium: "PF1.DistMedium",
    long: "PF1.DistLong",
    ft: "PF1.DistFt",
    mi: "PF1.DistMi",
    spec: "PF1.Special",
    seeText: "PF1.SeeText",
    unlimited: "PF1.Unlimited",
  },

  measureUnits: {
    ft: "PF1.DistFt",
    mi: "PF1.DistMi",
    m: "PF1.DistM",
    km: "PF1.DistKM",
  },

  measureUnitsShort: {
    ft: "PF1.DistFtShort",
    mi: "PF1.DistMiShort",
    m: "PF1.DistMShort",
    km: "PF1.DistKMShort",
  },

  actorStatures: {
    tall: "PF1.StatureTall",
    long: "PF1.StatureLong",
  },

  /* -------------------------------------------- */

  /**
   * This Object defines the types of single or area targets which can be applied in the game system.
   */
  targetTypes: {
    none: "PF1.None",
    self: "PF1.TargetSelf",
    creature: "PF1.TargetCreature",
    ally: "PF1.TargetAlly",
    enemy: "PF1.TargetEnemy",
    object: "PF1.TargetObject",
    space: "PF1.TargetSpace",
    radius: "PF1.TargetRadius",
    sphere: "PF1.TargetSphere",
    cylinder: "PF1.TargetCylinder",
    cone: "PF1.TargetCone",
    square: "PF1.TargetSquare",
    cube: "PF1.TargetCube",
    line: "PF1.TargetLine",
    wall: "PF1.TargetWall",
  },

  /* -------------------------------------------- */

  /**
   * This Object defines the various lengths of time which can occur in PF1
   */
  timePeriods: {
    inst: "PF1.TimeInst",
    turn: "PF1.TimeTurn",
    round: "PF1.TimeRound",
    minute: "PF1.TimeMinute",
    hour: "PF1.TimeHour",
    day: "PF1.TimeDay",
    month: "PF1.TimeMonth",
    year: "PF1.TimeYear",
    perm: "PF1.TimePerm",
    seeText: "PF1.SeeText",
    spec: "PF1.Special",
  },

  timePeriodsShort: {
    turn: "PF1.TimeTurnShort",
    round: "PF1.TimeRoundShort",
    minute: "PF1.TimeMinuteShort",
    hour: "PF1.TimeHourShort",
  },

  /* -------------------------------------------- */

  /**
   * This Object determines spells gained and cast per level
   */
  casterProgression: {
    castsPerDay: {
      prepared: {
        low: [
          [Number.POSITIVE_INFINITY],
          [Number.POSITIVE_INFINITY],
          [Number.POSITIVE_INFINITY],
          [Number.POSITIVE_INFINITY, 0],
          [Number.POSITIVE_INFINITY, 1],
          [Number.POSITIVE_INFINITY, 1],
          [Number.POSITIVE_INFINITY, 1, 0],
          [Number.POSITIVE_INFINITY, 1, 1],
          [Number.POSITIVE_INFINITY, 2, 1],
          [Number.POSITIVE_INFINITY, 2, 1, 0],
          [Number.POSITIVE_INFINITY, 2, 1, 1],
          [Number.POSITIVE_INFINITY, 2, 2, 1],
          [Number.POSITIVE_INFINITY, 3, 2, 1, 0],
          [Number.POSITIVE_INFINITY, 3, 2, 1, 1],
          [Number.POSITIVE_INFINITY, 3, 2, 2, 1],
          [Number.POSITIVE_INFINITY, 3, 3, 2, 1],
          [Number.POSITIVE_INFINITY, 4, 3, 2, 1],
          [Number.POSITIVE_INFINITY, 4, 3, 2, 2],
          [Number.POSITIVE_INFINITY, 4, 3, 3, 2],
          [Number.POSITIVE_INFINITY, 4, 4, 3, 3],
        ],
        med: [
          [Number.POSITIVE_INFINITY, 1],
          [Number.POSITIVE_INFINITY, 2],
          [Number.POSITIVE_INFINITY, 3],
          [Number.POSITIVE_INFINITY, 3, 1],
          [Number.POSITIVE_INFINITY, 4, 2],
          [Number.POSITIVE_INFINITY, 4, 3],
          [Number.POSITIVE_INFINITY, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 4, 4, 2],
          [Number.POSITIVE_INFINITY, 5, 4, 3],
          [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 5, 4, 4, 2],
          [Number.POSITIVE_INFINITY, 5, 5, 4, 3],
          [Number.POSITIVE_INFINITY, 5, 5, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 5, 5, 4, 4, 2],
          [Number.POSITIVE_INFINITY, 5, 5, 5, 4, 3],
          [Number.POSITIVE_INFINITY, 5, 5, 5, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 5, 5, 5, 4, 4, 2],
          [Number.POSITIVE_INFINITY, 5, 5, 5, 5, 4, 3],
          [Number.POSITIVE_INFINITY, 5, 5, 5, 5, 5, 4],
          [Number.POSITIVE_INFINITY, 5, 5, 5, 5, 5, 5],
        ],
        high: [
          [Number.POSITIVE_INFINITY, 1],
          [Number.POSITIVE_INFINITY, 2],
          [Number.POSITIVE_INFINITY, 2, 1],
          [Number.POSITIVE_INFINITY, 3, 2],
          [Number.POSITIVE_INFINITY, 3, 2, 1],
          [Number.POSITIVE_INFINITY, 3, 3, 2],
          [Number.POSITIVE_INFINITY, 4, 3, 2, 1],
          [Number.POSITIVE_INFINITY, 4, 3, 3, 2],
          [Number.POSITIVE_INFINITY, 4, 4, 3, 2, 1],
          [Number.POSITIVE_INFINITY, 4, 4, 3, 3, 2],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 3, 2, 1],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 3, 3, 2],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 3, 2, 1],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 3, 3, 2],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 3, 2, 1],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 3, 3, 2],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 3, 2, 1],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 3, 3, 2],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 4, 3, 3],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 4, 4, 4],
        ],
      },
      spontaneous: {
        low: [
          [Number.POSITIVE_INFINITY],
          [Number.POSITIVE_INFINITY],
          [Number.POSITIVE_INFINITY],
          [Number.POSITIVE_INFINITY, 1],
          [Number.POSITIVE_INFINITY, 1],
          [Number.POSITIVE_INFINITY, 1],
          [Number.POSITIVE_INFINITY, 1, 1],
          [Number.POSITIVE_INFINITY, 1, 1],
          [Number.POSITIVE_INFINITY, 2, 1],
          [Number.POSITIVE_INFINITY, 2, 1, 1],
          [Number.POSITIVE_INFINITY, 2, 1, 1],
          [Number.POSITIVE_INFINITY, 2, 2, 1],
          [Number.POSITIVE_INFINITY, 3, 2, 1, 1],
          [Number.POSITIVE_INFINITY, 3, 2, 1, 1],
          [Number.POSITIVE_INFINITY, 3, 2, 2, 1],
          [Number.POSITIVE_INFINITY, 3, 3, 2, 1],
          [Number.POSITIVE_INFINITY, 4, 3, 2, 1],
          [Number.POSITIVE_INFINITY, 4, 3, 2, 2],
          [Number.POSITIVE_INFINITY, 4, 3, 3, 2],
          [Number.POSITIVE_INFINITY, 4, 4, 3, 2],
        ],
        med: [
          [Number.POSITIVE_INFINITY, 1],
          [Number.POSITIVE_INFINITY, 2],
          [Number.POSITIVE_INFINITY, 3],
          [Number.POSITIVE_INFINITY, 3, 1],
          [Number.POSITIVE_INFINITY, 4, 2],
          [Number.POSITIVE_INFINITY, 4, 3],
          [Number.POSITIVE_INFINITY, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 4, 4, 2],
          [Number.POSITIVE_INFINITY, 5, 4, 3],
          [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 5, 4, 4, 2],
          [Number.POSITIVE_INFINITY, 5, 5, 4, 3],
          [Number.POSITIVE_INFINITY, 5, 5, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 5, 5, 4, 4, 2],
          [Number.POSITIVE_INFINITY, 5, 5, 5, 4, 3],
          [Number.POSITIVE_INFINITY, 5, 5, 5, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 5, 5, 5, 4, 4, 2],
          [Number.POSITIVE_INFINITY, 5, 5, 5, 5, 4, 3],
          [Number.POSITIVE_INFINITY, 5, 5, 5, 5, 5, 4],
          [Number.POSITIVE_INFINITY, 5, 5, 5, 5, 5, 5],
        ],
        high: [
          [Number.POSITIVE_INFINITY, 3],
          [Number.POSITIVE_INFINITY, 4],
          [Number.POSITIVE_INFINITY, 5],
          [Number.POSITIVE_INFINITY, 6, 3],
          [Number.POSITIVE_INFINITY, 6, 4],
          [Number.POSITIVE_INFINITY, 6, 5, 3],
          [Number.POSITIVE_INFINITY, 6, 6, 4],
          [Number.POSITIVE_INFINITY, 6, 6, 5, 3],
          [Number.POSITIVE_INFINITY, 6, 6, 6, 4],
          [Number.POSITIVE_INFINITY, 6, 6, 6, 5, 3],
          [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 4],
          [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 5, 3],
          [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 6, 4],
          [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 6, 5, 3],
          [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 6, 6, 4],
          [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 6, 6, 5, 3],
          [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 6, 6, 6, 4],
          [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 6, 6, 6, 5, 3],
          [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 6, 6, 6, 6, 4],
          [Number.POSITIVE_INFINITY, 6, 6, 6, 6, 6, 6, 6, 6, 6],
        ],
      },
      hybrid: {
        high: [
          [Number.POSITIVE_INFINITY, 2],
          [Number.POSITIVE_INFINITY, 3],
          [Number.POSITIVE_INFINITY, 4],
          [Number.POSITIVE_INFINITY, 4, 2],
          [Number.POSITIVE_INFINITY, 4, 3],
          [Number.POSITIVE_INFINITY, 4, 4, 2],
          [Number.POSITIVE_INFINITY, 4, 4, 3],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 2],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 3],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 2],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 3],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 2],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 3],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 2],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 3],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 4, 2],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 4, 3],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 4, 4, 2],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 4, 4, 3],
          [Number.POSITIVE_INFINITY, 4, 4, 4, 4, 4, 4, 4, 4, 4],
        ],
      },
      prestige: {
        low: [
          [Number.POSITIVE_INFINITY, 1],
          [Number.POSITIVE_INFINITY, 2],
          [Number.POSITIVE_INFINITY, 3],
          [Number.POSITIVE_INFINITY, 3, 1],
          [Number.POSITIVE_INFINITY, 4, 2],
          [Number.POSITIVE_INFINITY, 4, 3],
          [Number.POSITIVE_INFINITY, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 4, 4, 2],
          [Number.POSITIVE_INFINITY, 5, 4, 3],
          [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
          [Number.POSITIVE_INFINITY, 5, 4, 3, 1],
        ],
      },
    },
    spellsPreparedPerDay: {
      prepared: {
        low: [
          [null],
          [null],
          [null],
          [null, 0],
          [null, 1],
          [null, 1],
          [null, 1, 0],
          [null, 1, 1],
          [null, 2, 1],
          [null, 2, 1, 0],
          [null, 2, 1, 1],
          [null, 2, 2, 1],
          [null, 3, 2, 1, 0],
          [null, 3, 2, 1, 1],
          [null, 3, 2, 2, 1],
          [null, 3, 3, 2, 1],
          [null, 4, 3, 2, 1],
          [null, 4, 3, 2, 2],
          [null, 4, 3, 3, 2],
          [null, 4, 4, 3, 3],
        ],
        med: [
          [3, 1],
          [4, 2],
          [4, 3],
          [4, 3, 1],
          [4, 4, 2],
          [5, 4, 3],
          [5, 4, 3, 1],
          [5, 4, 4, 2],
          [5, 5, 4, 3],
          [5, 5, 4, 3, 1],
          [5, 5, 4, 4, 2],
          [5, 5, 5, 4, 3],
          [5, 5, 5, 4, 3, 1],
          [5, 5, 5, 4, 4, 2],
          [5, 5, 5, 5, 4, 3],
          [5, 5, 5, 5, 4, 3, 1],
          [5, 5, 5, 5, 4, 4, 2],
          [5, 5, 5, 5, 5, 4, 3],
          [5, 5, 5, 5, 5, 5, 4],
          [5, 5, 5, 5, 5, 5, 5],
        ],
        high: [
          [3, 1],
          [4, 2],
          [4, 2, 1],
          [4, 3, 2],
          [4, 3, 2, 1],
          [4, 3, 3, 2],
          [4, 4, 3, 2, 1],
          [4, 4, 3, 3, 2],
          [4, 4, 4, 3, 2, 1],
          [4, 4, 4, 3, 3, 2],
          [4, 4, 4, 4, 3, 2, 1],
          [4, 4, 4, 4, 3, 3, 2],
          [4, 4, 4, 4, 4, 3, 2, 1],
          [4, 4, 4, 4, 4, 3, 3, 2],
          [4, 4, 4, 4, 4, 4, 3, 2, 1],
          [4, 4, 4, 4, 4, 4, 3, 3, 2],
          [4, 4, 4, 4, 4, 4, 4, 3, 2, 1],
          [4, 4, 4, 4, 4, 4, 4, 3, 3, 2],
          [4, 4, 4, 4, 4, 4, 4, 4, 3, 3],
          [4, 4, 4, 4, 4, 4, 4, 4, 4, 4],
        ],
      },
      spontaneous: {
        low: [
          [2],
          [3],
          [4],
          [4, 2],
          [5, 3],
          [5, 4],
          [6, 4, 2],
          [6, 4, 3],
          [6, 5, 4],
          [6, 5, 4, 2],
          [6, 5, 4, 3],
          [6, 6, 5, 4],
          [6, 6, 5, 4, 2],
          [6, 6, 5, 4, 3],
          [6, 6, 6, 5, 4],
          [6, 6, 6, 5, 4],
          [6, 6, 6, 5, 4],
          [6, 6, 6, 6, 5],
          [6, 6, 6, 6, 5],
          [6, 6, 6, 6, 5],
        ],
        med: [
          [4, 2],
          [5, 3],
          [6, 4],
          [6, 4, 2],
          [6, 4, 3],
          [6, 4, 4],
          [6, 5, 4, 2],
          [6, 5, 4, 3],
          [6, 5, 4, 4],
          [6, 5, 5, 4, 2],
          [6, 6, 5, 4, 3],
          [6, 6, 5, 4, 4],
          [6, 6, 5, 5, 4, 2],
          [6, 6, 6, 5, 4, 3],
          [6, 6, 6, 5, 4, 4],
          [6, 6, 6, 5, 5, 4, 2],
          [6, 6, 6, 6, 5, 4, 3],
          [6, 6, 6, 6, 5, 4, 4],
          [6, 6, 6, 6, 5, 5, 4],
          [6, 6, 6, 6, 6, 5, 5],
        ],
        high: [
          [4, 2],
          [5, 2],
          [5, 3],
          [6, 3, 1],
          [6, 4, 2],
          [7, 4, 2, 1],
          [7, 5, 3, 2],
          [8, 5, 3, 2, 1],
          [8, 5, 4, 3, 2],
          [9, 5, 4, 3, 2, 1],
          [9, 5, 5, 4, 3, 2],
          [9, 5, 5, 4, 3, 2, 1],
          [9, 5, 5, 4, 4, 3, 2],
          [9, 5, 5, 4, 4, 3, 2, 1],
          [9, 5, 5, 4, 4, 4, 3, 2],
          [9, 5, 5, 4, 4, 4, 3, 2, 1],
          [9, 5, 5, 4, 4, 4, 3, 3, 2],
          [9, 5, 5, 4, 4, 4, 3, 3, 2, 1],
          [9, 5, 5, 4, 4, 4, 3, 3, 3, 2],
          [9, 5, 5, 4, 4, 4, 3, 3, 3, 3],
        ],
      },
      hybrid: {
        high: [
          [4, 2],
          [5, 2],
          [5, 3],
          [6, 3, 1],
          [6, 4, 2],
          [7, 4, 2, 1],
          [7, 5, 3, 2],
          [8, 5, 3, 2, 1],
          [8, 5, 4, 3, 2],
          [9, 5, 4, 3, 2, 1],
          [9, 5, 5, 4, 3, 2],
          [9, 5, 5, 4, 3, 2, 1],
          [9, 5, 5, 4, 4, 3, 2],
          [9, 5, 5, 4, 4, 3, 2, 1],
          [9, 5, 5, 4, 4, 4, 3, 2],
          [9, 5, 5, 4, 4, 4, 3, 2, 1],
          [9, 5, 5, 4, 4, 4, 3, 3, 2],
          [9, 5, 5, 4, 4, 4, 3, 3, 2, 1],
          [9, 5, 5, 4, 4, 4, 3, 3, 3, 2],
          [9, 5, 5, 4, 4, 4, 3, 3, 3, 3],
        ],
      },
      prestige: {
        low: [
          [null, 2],
          [null, 3],
          [null, 4],
          [null, 4, 2],
          [null, 4, 3],
          [null, 4, 4],
          [null, 5, 4, 2],
          [null, 5, 4, 3],
          [null, 5, 4, 4],
          [null, 5, 5, 4, 2],
          [null, 5, 5, 4, 2],
          [null, 5, 5, 4, 2],
          [null, 5, 5, 4, 2],
          [null, 5, 5, 4, 2],
          [null, 5, 5, 4, 2],
          [null, 5, 5, 4, 2],
          [null, 5, 5, 4, 2],
          [null, 5, 5, 4, 2],
          [null, 5, 5, 4, 2],
          [null, 5, 5, 4, 2],
        ],
      },
    },
  },

  /* -------------------------------------------- */

  // Healing Types
  /**
   * Types of healing
   */
  healingTypes: {
    healing: "PF1.Healing",
    temphp: "PF1.HealingTemp",
  },

  /* -------------------------------------------- */

  /**
   * Character senses options
   */
  senses: {
    bs: "PF1.SenseBS",
    dv: "PF1.SenseDV",
    ts: "PF1.SenseTS",
    tr: "PF1.SenseTR",
    ll: "PF1.SenseLL",
  },

  /* -------------------------------------------- */

  /**
   * The set of skill which can be trained in PF1
   */
  skills: {
    eng: "PF1.SkillEng",
    stl: "PF1.SkillStl",
    ath: "PF1.SkillAth",
    hnd: "PF1.SkillHnd",
    acr: "PF1.SkillAcr",
    lgr: "PF1.SkillLgr",
    end: "PF1.SkillEnd",
    srv: "PF1.SkillSrv",
    inv: "PF1.SkillInv",
    lor: "PF1.SkillLor",
    rsn: "PF1.SkillRsn",
    blf: "PF1.SkillBlf",
    dsc: "PF1.SkillDsc",
    arc: "PF1.SkillArc",
    hel: "PF1.SkillHel",
    imt: "PF1.SkillImt",
    cpt: "PF1.SkillCpt",
    dpl: "PF1.SkillDpl",
    prc: "PF1.SkillPrc",
    mys: "PF1.SkillMys",
  },

  /**
   * Compendium journal entries containing details about {@link skills}
   */
  skillCompendiumEntries: {
    eng: "pf1.skills.O7Utk8xKbHYDP7Ot",
    stl: "pf1.skills.T6nefKotIb5vKE9h",
    ath: "pf1.skills.W5GT9F3euC4aG9zP",
    hnd: "pf1.skills.fXauAodbOBHBi7Mj",
    acr: "pf1.skills.HPsdXjjRtKNGFGRc",
    lgr: "pf1.skills.xvua9V5Zggkc77Gt",
    end: "pf1.skills.L6LEhHRrn53cMuhE",
    srv: "pf1.skills.TXrxgrB2sQjppZFY",
    inv: "pf1.skills.CCy1fajKKU3K02iQ",
    lor: "pf1.skills.lP3L1RXi9q4ha9Gu",
    rsn: "pf1.skills.AOQsMtvLY4V3wFEL",
    blf: "pf1.skills.pAB2PYkZO3lsmeaC",
    dsc: "pf1.skills.rR877UB1xVd3Z7z1",
    arc: "pf1.skills.ImjhcPmm02QSCbPg",
    hel: "pf1.skills.RtBGttkocvTbySjd",
    imt: "pf1.skills.ki0QvL0K7u4YuK0O",
    cpt: "pf1.skills.QXIYXqy6ivNHrDrW",
    dpl: "pf1.skills.OtfbHJhIQsNzmiTN",
    prc: "pf1.skills.YYmmxDgp0g3ltSUG",
    mys: "pf1.skills.HhV5UmaRLTjtB3Xh",
  },

  /**
   * An array of {@link skills} that are considered background skills.
   */
  backgroundSkills: ["apr", "art", "crf", "han", "ken", "kge", "khi", "kno", "lin", "lor", "prf", "pro", "slt"],

  /* -------------------------------------------- */

  /**
   * Valid options for how a spell is prepared
   */
  spellPreparationModes: {
    atwill: "PF1.SpellPrepAtWill",
    prepared: "PF1.SpellPrepPrepared",
    spontaneous: "PF1.SpellPrepSpontaneous",
  },

  magicAuraByLevel: {
    spell: [
      { power: "faint", level: 1 },
      { power: "moderate", level: 4 },
      { power: "strong", level: 7 },
      { power: "overwhelming", level: 10 },
    ],
    item: [
      { power: "faint", level: 1 },
      { power: "moderate", level: 6 },
      { power: "strong", level: 12 },
      { power: "overwhelming", level: 21 },
    ],
  },

  auraStrengths: {
    1: "PF1.AuraStrength_Faint",
    2: "PF1.AuraStrength_Moderate",
    3: "PF1.AuraStrength_Strong",
    4: "PF1.AuraStrength_Overwhelming",
  },

  /* -------------------------------------------- */

  /* -------------------------------------------- */

  // Weapon Types
  weaponTypes: {
    melee: {
      _label: "PF1.WeaponTypeMelee",
      light: "PF1.WeaponPropLight",
      "1h": "PF1.WeaponPropOneHanded",
      "2h": "PF1.WeaponPropTwoHanded",
    },
    ranged: {
      _label: "PF1.WeaponTypeRanged",
      light: "PF1.WeaponPropLight",
      "1h": "PF1.WeaponPropOneHanded",
      "2h": "PF1.WeaponPropTwoHanded",
    },
    misc: {
      _label: "PF1.Misc",
      splash: "PF1.WeaponTypeSplash",
      other: "PF1.Other",
    },
  },

  // Weapon hold types
  weaponHoldTypes: {
    normal: "PF1.WeaponHoldTypeNormal",
    "2h": "PF1.WeaponHoldTypeTwoHanded",
    oh: "PF1.WeaponHoldTypeOffhand",
  },

  /* -------------------------------------------- */

  /**
   * Define the set of weapon arts
   */
  weaponArts: {
    brc: "PF1.WeaponArtBrace",
    cru: "PF1.WeaponArtCrushing",
    cur: "PF1.WeaponArtCurved",
    dea: "PF1.WeaponArtDeadly",
    dbl: "PF1.WeaponArtDouble",
    foc: "PF1.WeaponArtFocus",
    prj: "PF1.WeaponArtProjectile",
    rpd: "PF1.WeaponArtRapid",
    rch: "PF1.WeaponArtReach",
    thr: "PF1.WeaponArtThrown",
    vrs: "PF1.WeaponArtVersatile",
  },

  weaponArtDescriptions: {
    brc: "PF1.WeaponArtDescriptionBrace",
    cru: "PF1.WeaponArtDescriptionCrushing",
    cur: "PF1.WeaponArtDescriptionCurved",
    dea: "PF1.WeaponArtDescriptionDeadly",
    dbl: "PF1.WeaponArtDescriptionDouble",
    foc: "PF1.WeaponArtDescriptionFocus",
    prj: "PF1.WeaponArtDescriptionProjectile",
    rpd: "PF1.WeaponArtDescriptionRapid",
    rch: "PF1.WeaponArtDescriptionReach",
    thr: "PF1.WeaponArtDescriptionThrown",
    vrs: "PF1.WeaponArtDescriptionVersatile",
  },

  /**
   * Define the set of traits and their helper text
   */
  traitHelpers: {
    brc: "PF1.TraitHelperBrace",
    col: "PF1.TraitHelperColossal",
    cru: "PF1.TraitHelperCrushing",
    cur: "PF1.TraitHelperCurved",
    dea: "PF1.TraitHelperDeadly",
    dis: "PF1.TraitHelperDisarm",
    dbl: "PF1.TraitHelperDouble",
    fin: "PF1.TraitHelperFinesse",
    foc: "PF1.TraitHelperFocus",
    nnl: "PF1.TraitHelperNonLethal",
    pry: "PF1.TraitHelperParry",
    prj: "PF1.TraitHelperProjectile",
    rpd: "PF1.TraitHelperRapid",
    rch: "PF1.TraitHelperReach",
    thr: "PF1.TraitHelperThrown",
    trp: "PF1.TraitHelperTrip",
    vrs: "PF1.TraitHelperVersatile",
    air: "PF1.TraitHelperAir",
    ast: "PF1.TraitHelperAstral",
    bln: "PF1.TraitHelperBlinding",
    bom: "PF1.TraitHelperBooming",
    cnf: "PF1.TraitHelperConfusing",
    cmb: "PF1.TraitHelperCombat",
    crr: "PF1.TraitHelperCorrosive",
    ert: "PF1.TraitHelperEarth",
    eth: "PF1.TraitHelperEthereal",
    fir: "PF1.TraitHelperFire",
    frz: "PF1.TraitHelperFreezing",
    lgh: "PF1.TraitHelperLight",
    ser: "PF1.TraitHelperSearing",
    shd: "PF1.TraitHelperShadow",
    shc: "PF1.TraitHelperShocking",
    wtr: "PF1.TraitHelperWater",
    wth: "PF1.TraitHelperWithering",
  },

  /**
   * Define the set of weapon property flags which can exist on a weapon
   */
  weaponTraits: {
    brc: "PF1.WeaponPropBrace",
    col: "PF1.WeaponPropColossal",
    cru: "PF1.WeaponPropCrushing",
    cur: "PF1.WeaponPropCurved",
    dea: "PF1.WeaponPropDeadly",
    dis: "PF1.WeaponPropDisarm",
    dbl: "PF1.WeaponPropDouble",
    fin: "PF1.WeaponPropFinesse",
    foc: "PF1.WeaponPropFocus",
    nnl: "PF1.WeaponPropNonLethal",
    pry: "PF1.WeaponPropParry",
    prj: "PF1.WeaponPropProjectile",
    rpd: "PF1.WeaponPropRapid",
    rch: "PF1.WeaponPropReach",
    thr: "PF1.WeaponPropThrown",
    trp: "PF1.WeaponPropTrip",
    vrs: "PF1.WeaponPropVersatile",
  },

  /**
   * Define the set of spell property flags which can exist on a spell
   */
  spellTraits: {
    air: "PF1.SpellPropAir",
    ast: "PF1.SpellPropAstral",
    cmb: "PF1.SpellPropCombat",
    ert: "PF1.SpellPropEarth",
    eth: "PF1.SpellPropEthereal",
    fir: "PF1.SpellPropFire",
    lgh: "PF1.SpellPropLight",
    nat: "PF1.SpellPropNatural",
    shd: "PF1.SpellPropShadow",
    wtr: "PF1.SpellPropWater",
  },

  /**
   * Define the set of element crit effects
   */
   elementCritTraits: {
    bln: "PF1.SpellPropBlinding",
    bom: "PF1.SpellPropBooming",
    cnf: "PF1.SpellPropConfusing",
    crr: "PF1.SpellPropCorrosive",
    frz: "PF1.SpellPropFreezing",
    ser: "PF1.SpellPropSearing",
    shc: "PF1.SpellPropShocking",
    wth: "PF1.SpellPropWithering",
  },

  /* -------------------------------------------- */

  /**
   * The components required for casting a spell
   */
  spellComponents: {
    V: "PF1.SpellComponentVerbal",
    S: "PF1.SpellComponentSomatic",
    T: "PF1.SpellComponentThought",
    E: "PF1.SpellComponentEmotion",
    M: "PF1.SpellComponentMaterial",
    F: "PF1.SpellComponentFocus",
    DF: "PF1.SpellComponentDivineFocus",
  },

  /**
   * Spell planes
   */
  spellPlanes: {
    earth: "PF1.SpellPlaneEarth",
    fire: "PF1.SpellPlaneFire",
    water: "PF1.SpellPlaneWater",
    air: "PF1.SpellPlaneAir",
    light: "PF1.SpellPlaneLight",
    shadow: "PF1.SpellPlaneShadow",
    astral: "PF1.SpellPlaneAstral",
    ethereal: "PF1.SpellPlaneEthereal",
    natural: "PF1.SpellPlaneNatural",
    misc: "PF1.Misc",
  },

  /**
   * Spell levels
   */
  spellLevels: {
    0: "PF1.SpellLevel0",
    1: "PF1.SpellLevel1",
    2: "PF1.SpellLevel2",
    3: "PF1.SpellLevel3",
    4: "PF1.SpellLevel4",
    5: "PF1.SpellLevel5",
    6: "PF1.SpellLevel6",
    7: "PF1.SpellLevel7",
    8: "PF1.SpellLevel8",
    9: "PF1.SpellLevel9",
    10: "PF1.SpellLevel10",
  },

  /**
   * Spellbooks
   */
   spellBooks: {
    arcane: "PF1.Arcane",
    divine: "PF1.Divine",
    "": "PF1.Universal",
  },

  /* -------------------------------------------- */

  /**
   * Weapon proficiency levels
   * Each level provides a proficiency multiplier
   */
  proficiencyLevels: {
    "-4": "Not Proficient",
    0: "Proficient",
  },

  /* -------------------------------------------- */

  conditionTypes: {
    blind: "PF1.CondTypeBlind",
    confuse: "PF1.CondTypeConfuse",
    daze: "PF1.CondTypeDaze",
    dazzle: "PF1.CondTypeDazzle",
    deaf: "PF1.CondTypeDeaf",
    deathEffects: "PF1.CondTypeDeathEffects",
    disease: "PF1.CondTypeDisease",
    energyDrain: "PF1.CondTypeEnergyDrain",
    fatigue: "PF1.CondTypeFatigue",
    fear: "PF1.CondTypeFear",
    mindAffecting: "PF1.CondTypeMindAffecting",
    poison: "PF1.CondTypePoison",
    sicken: "PF1.CondTypeSicken",
    paralyze: "PF1.CondTypeParalyze",
    persistentDamage: "PF1.CondTypePersistentDamage",
    persistentHealing: "PF1.CondTypePersistentHealing",
    petrify: "PF1.CondTypePetrify",
    stun: "PF1.CondTypeStun",
    sleep: "PF1.CondTypeSleep",
  },

  conditions: {
    pf1_blind: "PF1.CondBlind",
    concentrating: "PF1.CondConcentrating",
    confused: "PF1.CondConfused",
    cowering: "PF1.CondCowering",
    dazed: "PF1.CondDazed",
    dazzled: "PF1.CondDazzled",
    pf1_deaf: "PF1.CondDeaf",
    entangled: "PF1.CondEntangled",
    exhausted: "PF1.CondExhausted",
    fascinated: "PF1.CondFascinated",
    fatigued: "PF1.CondFatigued",
    frightened: "PF1.CondFrightened",
    grappled: "PF1.CondGrappled",
    helpless: "PF1.CondHelpless",
    incorporeal: "PF1.CondIncorporeal",
    invisible: "PF1.CondInvisible",
    nauseated: "PF1.CondNauseated",
    panicked: "PF1.CondPanicked",
    paralyzed: "PF1.CondParalyzed",
    persistentDamage: "PF1.CondPersistentDamage",
    persistentHealing: "PF1.CondPersistentHealing",
    petrified: "PF1.CondPetrified",
    pinned: "PF1.CondPinned",
    pf1_prone: "PF1.CondProne",
    shaken: "PF1.CondShaken",
    sickened: "PF1.CondSickened",
    pf1_sleep: "PF1.CondSleep",
    staggered: "PF1.CondStaggered",
    stunned: "PF1.CondStunned",
    susceptible: "PF1.CondSusceptible",
  },

  conditionTextures: {
    pf1_blind: "systems/pf1/icons/conditions/blind.png",
    concentrating: "systems/pf1/icons/conditions/brain.png",
    confused: "systems/pf1/icons/conditions/confused.png",
    cowering: "systems/pf1/icons/conditions/cowering.png",
    dazed: "systems/pf1/icons/conditions/dazed.png",
    dazzled: "systems/pf1/icons/conditions/dazzled.png",
    pf1_deaf: "systems/pf1/icons/conditions/deaf.png",
    entangled: "systems/pf1/icons/conditions/entangled.png",
    exhausted: "systems/pf1/icons/conditions/exhausted.png",
    fascinated: "systems/pf1/icons/conditions/fascinated.png",
    fatigued: "systems/pf1/icons/conditions/fatigued.png",
    frightened: "systems/pf1/icons/conditions/frightened.png",
    grappled: "systems/pf1/icons/conditions/grappled.png",
    helpless: "systems/pf1/icons/conditions/helpless.png",
    incorporeal: "systems/pf1/icons/conditions/incorporeal.png",
    invisible: "systems/pf1/icons/conditions/invisible.png",
    nauseated: "systems/pf1/icons/conditions/nauseated.png",
    panicked: "systems/pf1/icons/conditions/fear.png",
    paralyzed: "systems/pf1/icons/conditions/paralyzed.png",
    persistentDamage: "systems/pf1/icons/conditions/bleed.png",
    persistentHealing: "systems/pf1/icons/conditions/healing.png",
    petrified: "systems/pf1/icons/conditions/petrified.png",
    pinned: "systems/pf1/icons/conditions/pinned.png",
    pf1_prone: "systems/pf1/icons/conditions/prone.png",
    shaken: "systems/pf1/icons/conditions/shaken.png",
    sickened: "systems/pf1/icons/conditions/sickened.png",
    pf1_sleep: "systems/pf1/icons/conditions/sleep.png",
    staggered: "systems/pf1/icons/conditions/staggered.png",
    stunned: "systems/pf1/icons/conditions/stunned.png",
    susceptible: "systems/pf1/icons/conditions/hazard.png",
  },

  conditionCompendiumEntries: {
    pf1_blind: "pf1.conditions.coxrkb6qUKBZvVxP",
    concentrating: "pf1.conditions.jQLJg0XQq4rx6huv",
    confused: "pf1.conditions.VbG9W9dMz2jhgfCi",
    cowering: "pf1.conditions.M9FWe38D2J740wLH",
    dazed: "pf1.conditions.woFU0s24URgixLm7",
    dazzled: "pf1.conditions.2pICNUG3g7O4obyu",
    pf1_deaf: "pf1.conditions.ZTA39rR8AnZcydiu",
    entangled: "pf1.conditions.WH3Hop5fUDUzVUVg",
    exhausted: "pf1.conditions.I1cKXhfBaFIrL9Ix",
    fascinated: "pf1.conditions.fDeN1Ze5F2uCiUPj",
    fatigued: "pf1.conditions.nku8mgRBNt0iXqzB",
    frightened: "pf1.conditions.QdwliEzfA3ZK9YtS",
    grappled: "pf1.conditions.zemoWcP2SR6FTiS1",
    helpless: "pf1.conditions.g84rPrlfDnkUopAa",
    incorporeal: "pf1.conditions.75ojKVcFLHBi80J9",
    invisible: "pf1.conditions.L3hNCX9kLGlmoQhc",
    nauseated: "pf1.conditions.1fkE2juoZHXjU90M",
    panicked: "pf1.conditions.xhQma4Gl4wLO0jnZ",
    paralyzed: "pf1.conditions.8zG4yDD2zGVSAJFH",
    persistentDamage: "pf1.conditions.93aTFy7v8VUq8sNi",
    persistentHealing: "pf1.conditions.93aTFy7v8VUq8sNi",
    petrified: "pf1.conditions.qLQsdbMorUOjOJWJ",
    pinned: "pf1.conditions.uxkaA2RC6METVN6w",
    pf1_prone: "pf1.conditions.Av5KcblR1Wd68uWY",
    shaken: "pf1.conditions.07JPXUqIPshVnTCL",
    sickened: "pf1.conditions.0AJZKIpP3lS3FVKa",
    pf1_sleep: "pf1.conditions.bj36eCfZGJG5AGTQ",
    staggered: "pf1.conditions.6LpiJdKskDFD4zLC",
    stunned: "pf1.conditions.assqfdN6G1URo9MZ",
    susceptible: "pf1.conditions.assqfdN6G1URo9MX",
  },

  conditionSusceptible: {
    acid: "PF1.CondSusceptibleAcid",
    cold: "PF1.CondSusceptibleCold",
    electric: "PF1.CondSusceptibleElectric",
    fire: "PF1.CondSusceptibleFire",
    force: "PF1.CondSusceptibleForce",
    psychic: "PF1.CondSusceptiblePsychic",
    radiant: "PF1.CondSusceptibleRadiant",
    necrotic: "PF1.CondSusceptibleNecrotic",
  },

  conditionSusceptibleTextures: {
    acid: "systems/pf1/icons/conditions/susceptible_acid.png",
    cold: "systems/pf1/icons/conditions/susceptible_cold.png",
    electric: "systems/pf1/icons/conditions/susceptible_electric.png",
    fire: "systems/pf1/icons/conditions/susceptible_fire.png",
    force: "systems/pf1/icons/conditions/susceptible_force.png",
    psychic: "systems/pf1/icons/conditions/susceptible_psychic.png",
    radiant: "systems/pf1/icons/conditions/susceptible_radiant.png",
    necrotic: "systems/pf1/icons/conditions/susceptible_necrotic.png",
  }, 
   
  buffTypes: {
    temp: "PF1.Temporary",
    perm: "PF1.Permanent",
    item: "PF1.Item",
    misc: "PF1.Misc",
  },

  /**
   * Dictionaries of conditional modifier targets, each with a label and sub-categories
   */
  conditionalTargets: {
    attack: {
      _label: "PF1.AttackRollPlural",
      allAttack: "PF1.All",
      hasteAttack: "PF1.Haste",
      rapidShotAttack: "PF1.RapidShot",
    },
    damage: {
      _label: "PF1.Damage",
      allDamage: "PF1.All",
      hasteDamage: "PF1.Haste",
      rapidShotDamage: "PF1.RapidShot",
    },
    size: {
      _label: "PF1.Size",
    },
    effect: {
      _label: "PF1.Effects",
    },
    misc: {
      _label: "PF1.MiscShort",
    },
  },

  /**
   * Dictionaries of change/buff targets, each with a label and a category it belongs to,
   * as well as a sort value that determines this buffTarget's priority when Changes are applied.
   */
  buffTargets: {
    acpA: { label: "PF1.ACPArmor", category: "misc", sort: 10000 },
    acpS: { label: "PF1.ACPShield", category: "misc", sort: 11000 },
    mDexA: { label: "PF1.MaxDexArmor", category: "misc", sort: 20000 },
    mDexS: { label: "PF1.MaxDexShield", category: "misc", sort: 21000 },
    str: { label: "PF1.AbilityStr", category: "ability", sort: 30000 },
    dex: { label: "PF1.AbilityDex", category: "ability", sort: 31000 },
    sta: { label: "PF1.AbilitySta", category: "ability", sort: 32000 },
    int: { label: "PF1.AbilityInt", category: "ability", sort: 33000 },
    wis: { label: "PF1.AbilityWis", category: "ability", sort: 34000 },
    mna: { label: "PF1.AbilityMna", category: "ability", sort: 35000 },
    prs: { label: "PF1.AbilityPrs", category: "ability", sort: 36000 },
    cnv: { label: "PF1.AbilityCnv", category: "ability", sort: 37000 },
    fth: { label: "PF1.AbilityFth", category: "ability", sort: 38000 },
    strMod: { label: "PF1.AbilityStrMod", category: "ability", sort: 40000 },
    dexMod: { label: "PF1.AbilityDexMod", category: "ability", sort: 41000 },
    staMod: { label: "PF1.AbilityStaMod", category: "ability", sort: 42000 },
    intMod: { label: "PF1.AbilityIntMod", category: "ability", sort: 43000 },
    wisMod: { label: "PF1.AbilityWisMod", category: "ability", sort: 44000 },
    mnaMod: { label: "PF1.AbilityMnaMod", category: "ability", sort: 45000 },
    prsMod: { label: "PF1.AbilityPrsMod", category: "ability", sort: 46000 },
    cnvMod: { label: "PF1.AbilityCnvMod", category: "ability", sort: 47000 },
    fthMod: { label: "PF1.AbilityFthMod", category: "ability", sort: 48000 }, 
    bdy: { label: "PF1.AspectBdy", category: "aspect", sort: 50000 },
    mnd: { label: "PF1.AspectMnd", category: "aspect", sort: 51000 },
    spr: { label: "PF1.AspectSpr", category: "aspect", sort: 51000 },
    skills: { label: "PF1.BuffTarAllSkills", category: "skills", sort: 50000 },
    bdySkills: { label: "PF1.BuffTarBdySkills", category: "skills", sort: 60100 },
    strSkills: { label: "PF1.BuffTarStrSkills", category: "skills", sort: 60200 },
    dexSkills: { label: "PF1.BuffTarDexSkills", category: "skills", sort: 60300 },
    staSkills: { label: "PF1.BuffTarStaSkills", category: "skills", sort: 60400 },
    mndSkills: { label: "PF1.BuffTarMndSkills", category: "skills", sort: 60500 },
    intSkills: { label: "PF1.BuffTarIntSkills", category: "skills", sort: 60600 },
    wisSkills: { label: "PF1.BuffTarWisSkills", category: "skills", sort: 60700 },
    mnaSkills: { label: "PF1.BuffTarMnaSkills", category: "skills", sort: 60800 },
    sprSkills: { label: "PF1.BuffTarSprSkills", category: "skills", sort: 60900 },
    prsSkills: { label: "PF1.BuffTarPrsSkills", category: "skills", sort: 61000 },
    cnvSkills: { label: "PF1.BuffTarCnvSkills", category: "skills", sort: 61100 },
    fthSkills: { label: "PF1.BuffTarFthSkills", category: "skills", sort: 61200 },
    allChecks: { label: "PF1.BuffTarAllAbilityChecks", category: "abilityChecks", sort: 70000 },
    strChecks: { label: "PF1.BuffTarStrChecks", category: "abilityChecks", sort: 80000 },
    dexChecks: { label: "PF1.BuffTarDexChecks", category: "abilityChecks", sort: 81000 },
    staChecks: { label: "PF1.BuffTarStaChecks", category: "abilityChecks", sort: 82000 },
    intChecks: { label: "PF1.BuffTarIntChecks", category: "abilityChecks", sort: 83000 },
    wisChecks: { label: "PF1.BuffTarWisChecks", category: "abilityChecks", sort: 84000 },
    mnaChecks: { label: "PF1.BuffTarMnaChecks", category: "abilityChecks", sort: 85000 },
    prsChecks: { label: "PF1.BuffTarPrsChecks", category: "abilityChecks", sort: 86000 },
    cnvChecks: { label: "PF1.BuffTarCnvChecks", category: "abilityChecks", sort: 87000 },
    fthChecks: { label: "PF1.BuffTarFthChecks", category: "abilityChecks", sort: 88000 },
    allSpeeds: { label: "PF1.BuffTarAllSpeeds", category: "speed", sort: 90000 },
    landSpeed: { label: "PF1.SpeedLand", category: "speed", sort: 91000 },
    climbSpeed: { label: "PF1.SpeedClimb", category: "speed", sort: 92000 },
    swimSpeed: { label: "PF1.SpeedSwim", category: "speed", sort: 93000 },
    burrowSpeed: { label: "PF1.SpeedBurrow", category: "speed", sort: 94000 },
    flySpeed: { label: "PF1.SpeedFly", category: "speed", sort: 95000 },
    ac: { label: "PF1.BuffTarACGeneric", category: "defense", sort: 100000 },
    aac: { label: "PF1.BuffTarACArmor", category: "defense", sort: 101000 },
    sac: { label: "PF1.BuffTarACShield", category: "defense", sort: 102000 },
    tac: { label: "PF1.BuffTarACTouch", category: "defense", sort: 103000 },
    ffac: { label: "PF1.BuffTarACFlatFooted", category: "defense", sort: 104000 },
    bab: { label: "PF1.BAB", category: "attack", sort: 110000 },
    attack: { label: "PF1.BuffTarAllAttackRolls", category: "attack", sort: 111000 },
    "~attackCore": { label: "", category: "attack", sort: 112000 },
    mattack: { label: "PF1.BuffTarMeleeAttack", category: "attack", sort: 113000 },
    rattack: { label: "PF1.BuffTarRangedAttack", category: "attack", sort: 114000 },
    damage: { label: "PF1.BuffTarAllDamageRolls", category: "damage", sort: 120000 },
    sdamage: { label: "PF1.SpellDamage", category: "damage", sort: 121000 },
    mdamage: { label: "PF1.MeleeWeaponDamage", category: "damage", sort: 122000 },
    rdamage: { label: "PF1.RangedWeaponDamage", category: "damage", sort: 123000 },
    critConfirm: { label: "PF1.CriticalConfirmation", category: "attack", sort: 130000 },
    allSavingThrows: { label: "PF1.BuffTarAllSavingThrows", category: "savingThrows", sort: 140000 },
    bdySave: { label: "PF1.SavingThrowBdy", category: "savingThrows", sort: 141000 },
    mndSave: { label: "PF1.SavingThrowMnd", category: "savingThrows", sort: 142000 },
    sprSave: { label: "PF1.SavingThrowSpr", category: "savingThrows", sort: 143000 },
    cmb: { label: "PF1.CMB", category: "attack", sort: 150000 },
    cmd: { label: "PF1.CMD", category: "defense", sort: 151000 },
    init: { label: "PF1.Initiative", category: "misc", sort: 160000 },
    mhp: { label: "PF1.HitPoints", category: "health", sort: 170000 },
    resolve: { label: "PF1.Resolve", category: "health", sort: 171000 },   
    spellResist: { label: "PF1.SpellResistance", category: "defense", sort: 180000 },
    bonusSkillRanks: { label: "PF1.BuffTarBonusSkillRanks", category: "skills", sort: 190000 },
    bonusFeats: { label: "PF1.BuffTarBonusFeats", category: "misc", sort: 200000 },
    bonusCarryCapacity: { label: "PF1.BuffTarBonusCarryCapacity", category: "misc", sort: 210000 },
    artificerCreations: { label: "PF1.BuffTarArtificerCreations", category: "feat", sort: 220000 },
    channeling: { label: "PF1.BuffTarChanneling", category: "feat", sort: 230000 },
    eldritchBlast: { label: "PF1.BuffTarEldritchBlast", category: "feat", sort: 240000 },
    grenadier: { label: "PF1.BuffTarGrenadier", category: "feat", sort: 250000 },
    inspiration: { label: "PF1.BuffTarInspiration", category: "feat", sort: 260000 },
    monastic: { label: "PF1.BuffTarMonastic", category: "feat", sort: 270000 },
    rage: { label: "PF1.BuffTarRage", category: "feat", sort: 280000 },
    suddenStrike: { label: "PF1.BuffTarSuddenStrike", category: "feat", sort: 290000 },
    occultTier: { label: "PF1.BuffTarOccultTier", category: "casting", sort: 300000 },
    arcaneTier: { label: "PF1.BuffTarArcaneTier", category: "casting", sort: 310000 },
    arcanePool: { label: "PF1.BuffTarArcanePool", category: "casting", sort: 311000 },
    divineTier: { label: "PF1.BuffTarDivineTier", category: "casting", sort: 320000 },
    concentration: { label: "PF1.BuffTarConc", category: "casting", sort: 330000 },
  },

  buffTargetCategories: {
    defense: { label: "PF1.Defense" },
    savingThrows: { label: "PF1.SavingThrowPlural" },
    attack: { label: "PF1.Attack" },
    damage: { label: "PF1.Damage" },
    ability: { label: "PF1.AbilityScore" },
    aspect: { label: "PF1.Aspect" },
    abilityChecks: { label: "PF1.BuffTarAbilityChecks" },
    skills: { label: "PF1.Skills" },
    skill: { label: "PF1.BuffTarSpecificSkill" },
    speed: { label: "PF1.Speed" },
    health: { label: "PF1.Health" },
    misc: { label: "PF1.Misc" },
    feat: { label: "PF1.Feats" },
    casting: { label: "PF1.Casting" },
  },

  contextNoteTargets: {
    attack: { label: "PF1.AttackRollPlural", category: "attacks" },
    effect: { label: "PF1.Effects", category: "attacks" },
    melee: { label: "PF1.Melee", category: "attacks" },
    meleeWeapon: { label: "PF1.MeleeWeapon", category: "attacks" },
    meleeSpell: { label: "PF1.MeleeSpell", category: "attacks" },
    ranged: { label: "PF1.Ranged", category: "attacks" },
    rangedWeapon: { label: "PF1.RangedWeapon", category: "attacks" },
    rangedSpell: { label: "PF1.RangedSpell", category: "attacks" },
    cmb: { label: "PF1.CMB", category: "attacks" },
    allSavingThrows: { label: "PF1.BuffTarAllSavingThrows", category: "savingThrows" },
    bdySave: { label: "PF1.SavingThrowBdy", category: "savingThrows" },
    mndSave: { label: "PF1.SavingThrowMnd", category: "savingThrows" },
    sprSave: { label: "PF1.SavingThrowSpr", category: "savingThrows" },
    skills: { label: "PF1.BuffTarAllSkills", category: "skills" },
    bdySkills: { label: "PF1.BuffTarBdySkills", category: "skills" },
    strSkills: { label: "PF1.BuffTarStrSkills", category: "skills" },
    dexSkills: { label: "PF1.BuffTarDexSkills", category: "skills" },
    staSkills: { label: "PF1.BuffTarStaSkills", category: "skills" },
    mndSkills: { label: "PF1.BuffTarMndSkills", category: "skills" },
    intSkills: { label: "PF1.BuffTarIntSkills", category: "skills" },
    wisSkills: { label: "PF1.BuffTarWisSkills", category: "skills" },
    mnaSkills: { label: "PF1.BuffTarMnaSkills", category: "skills" },
    sprSkills: { label: "PF1.BuffTarSprSkills", category: "skills" },
    prsSkills: { label: "PF1.BuffTarPrsSkills", category: "skills" },
    cnvSkills: { label: "PF1.BuffTarCnvSkills", category: "skills" },
    fthSkills: { label: "PF1.BuffTarFthSkills", category: "skills" },
    allChecks: { label: "PF1.BuffTarAllAbilityChecks", category: "abilityChecks" },
    strChecks: { label: "PF1.BuffTarStrChecks", category: "abilityChecks" },
    dexChecks: { label: "PF1.BuffTarDexChecks", category: "abilityChecks" },
    staChecks: { label: "PF1.BuffTarStaChecks", category: "abilityChecks" },
    intChecks: { label: "PF1.BuffTarIntChecks", category: "abilityChecks" },
    wisChecks: { label: "PF1.BuffTarWisChecks", category: "abilityChecks" },
    mnaChecks: { label: "PF1.BuffTarMnaChecks", category: "abilityChecks" },
    prsChecks: { label: "PF1.BuffTarPrsChecks", category: "abilityChecks" },
    cnvChecks: { label: "PF1.BuffTarCnvChecks", category: "abilityChecks" },
    fthChecks: { label: "PF1.BuffTarFthChecks", category: "abilityChecks" },
    spellEffect: { label: "PF1.SpellBuffEffect", category: "spell" },
    concentration: { label: "PF1.Concentration", category: "spell" },
    cl: { label: "PF1.CasterLevel", category: "spell" },
    ac: { label: "PF1.ACNormal", category: "defense" },
    cmd: { label: "PF1.CMD", category: "defense" },
    sr: { label: "PF1.SpellResistance", category: "defense" },
    init: { label: "PF1.Initiative", category: "misc" },
  },

  contextNoteCategories: {
    attacks: { label: "PF1.Attacks" },
    savingThrows: { label: "PF1.SavingThrowPlural" },
    skills: { label: "PF1.Skills" },
    skill: { label: "PF1.BuffTarSpecificSkill" },
    abilityChecks: { label: "PF1.BuffTarAbilityChecks" },
    spell: { label: "PF1.BuffTarSpells" },
    defense: { label: "PF1.Defense" },
    misc: { label: "PF1.Misc" },
  },

  /**
   * A list of Golarion's languages
   */
  languages: {
    aboleth: "PF1.LanguageAboleth",
    abyssal: "PF1.LanguageAbyssal",
    aklo: "PF1.LanguageAklo",
    ancientosiriani: "PF1.LanguageAncientOsiriani",
    aquan: "PF1.LanguageAquan",
    auran: "PF1.LanguageAuran",
    azlanti: "PF1.LanguageAzlanti",
    boggard: "PF1.LanguageBoggard",
    celestial: "PF1.LanguageCelestial",
    common: "PF1.LanguageCommon",
    cyclops: "PF1.LanguageCyclops",
    dark: "PF1.LanguageDark",
    draconic: "PF1.LanguageDraconic",
    drowsign: "PF1.LanguageDrowsign",
    druidic: "PF1.LanguageDruidic",
    dwarven: "PF1.LanguageDwarven",
    dziriak: "PF1.LanguageDziriak",
    elven: "PF1.LanguageElven",
    giant: "PF1.LanguageGiant",
    gnoll: "PF1.LanguageGnoll",
    gnome: "PF1.LanguageGnome",
    goblin: "PF1.LanguageGoblin",
    grippli: "PF1.LanguageGrippli",
    halfling: "PF1.LanguageHalfling",
    hallit: "PF1.LanguageHallit",
    ignan: "PF1.LanguageIgnan",
    jistka: "PF1.LanguageJistka",
    infernal: "PF1.LanguageInfernal",
    kelish: "PF1.LanguageKelish",
    necril: "PF1.LanguageNecril",
    orc: "PF1.LanguageOrc",
    orvian: "PF1.LanguageOrvian",
    osiriani: "PF1.LanguageOsiriani",
    polyglot: "PF1.LanguagePolyglot",
    protean: "PF1.LanguageProtean",
    shadowtongue: "PF1.LanguageShadowTongue",
    shoanti: "PF1.LanguageShoanti",
    skald: "PF1.LanguageSkald",
    sphinx: "PF1.LanguageSphinx",
    sylvan: "PF1.LanguageSylvan",
    taldane: "PF1.LanguageTaldane",
    tekritanin: "PF1.LanguageTekritanin",
    tengu: "PF1.LanguageTengu",
    terran: "PF1.LanguageTerran",
    thassilonian: "PF1.LanguageThassilonian",
    tien: "PF1.LanguageTien",
    treant: "PF1.LanguageTreant",
    undercommon: "PF1.LanguageUndercommon",
    varisian: "PF1.LanguageVarisian",
    vegepygmy: "PF1.LanguageVegepygmy",
    vudrani: "PF1.LanguageVudrani",
  },

  /**
   * Creature types
   */
  creatureTypes: {
    aberration: "PF1.CreatureTypeAberration",
    beast: "PF1.CreatureTypeBeast",
    construct: "PF1.CreatureTypeConstruct",
    dragon: "PF1.CreatureTypeDragon",
    elemental: "PF1.CreatureTypeElemental",
    fey: "PF1.CreatureTypeFey",
    humanoid: "PF1.CreatureTypeHumanoid",
    monstrousHumanoid: "PF1.CreatureTypeMonstrousHumanoid",
    ooze: "PF1.CreatureTypeOoze",
    outsider: "PF1.CreatureTypeOutsider",
    plant: "PF1.CreatureTypePlant",
    swarm: "PF1.CreatureTypeSwarm",
    undead: "PF1.CreatureTypeUndead",
    vermin: "PF1.CreatureTypeVermin",
  },

  /**
   * Creature types description
   */
   creatureTypesDescription: {
    aberration: "PF1.CreatureTypeDescriptionAberration",
    beast: "PF1.CreatureTypeDescriptionBeast",
    construct: "PF1.CreatureTypeDescriptionConstruct",
    dragon: "PF1.CreatureTypeDescriptionDragon",
    elemental: "PF1.CreatureTypeDescriptionElemental",
    fey: "PF1.CreatureTypeDescriptionFey",
    humanoid: "PF1.CreatureTypeDescriptionHumanoid",
    monstrousHumanoid: "PF1.CreatureTypeDescriptionMonstrousHumanoid",
    ooze: "PF1.CreatureTypeDescriptionOoze",
    outsider: "PF1.CreatureTypeDescriptionOutsider",
    plant: "PF1.CreatureTypeDescriptionPlant",
    swarm: "PF1.CreatureTypeDescriptionSwarm",
    undead: "PF1.CreatureTypeDescriptionUndead",
    vermin: "PF1.CreatureTypeDescriptionVermin",
  },

  spellRangeFormulas: {
    close: "5 + floor(@cl / 4) * 5",
    short: "25 + floor(@cl / 2) * 5",
    medium: "50 + @cl * 5",
    long: "100 + @cl * 10",
  },

  /**
   * An array containing the damage dice progression for size adjustments
   */
  sizeDie: [
    "1",
    "1d2",
    "1d3",
    "1d4",
    "1d6",
    "1d8",
    "1d10",
    "2d6",
    "2d8",
    "3d6",
    "3d8",
    "4d6",
    "4d8",
    "6d6",
    "6d8",
    "8d6",
    "8d8",
    "12d6",
    "12d8",
    "16d6",
    "16d8",
  ],

  /**
   * An array containing the damage dice progression for sides
   */
  dieSides: [1, 2, 3, 4, 6, 8, 10, 12],

  /**
   * Arrays of Character Level XP Requirements by XP track
   */
  CHARACTER_EXP_LEVELS: [
    0,
    3000,
    7000,
    12000,
    18000,
    25000,
    33000,
    42000,
    52000,
    63000,
    75000,
    88000,
    102000,
    117000,
    133000,
    150000,
    168000,
    187000,
    207000,
    228000,
    250000
  ],

  /**
   * An array of Challenge Rating XP Levels
   */
  CR_EXP_LEVELS: [
    200,
    400,
    600,
    800,
    1200,
    1600,
    2400,
    3200,
    4800,
    6400,
    9600,
    12800,
    19200,
    25600,
    38400,
    51200,
    76800,
    102400,
    153600,
    204800,
    307200,
    409600,
    614400,
    819200,
    1228800,
    1638400,
    2457600,
    3276800,
    4915200,
    6553600,
    9830400,
  ],

  temporaryRollDataFields: {
    actor: [
      "d20",
      "attackBonus",
      "damageBonus",
      "pointBlankBonus",
      "rapidShotPenalty",
      "powerAttackBonus",
      "powerAttackPenalty",
      "conditionals",
      "concentrationBonus",
      "formulaBonus",
      "dcBonus",
      "chargeCostBonus",
      "chargeCost",
      "sizeBonus",
      "bonus",
      "critMult",
      "ablMult",
      "ablDamage",
      "cl",
      "sl",
      "ablMod",
      "item",
      "level",
      "mod",
    ],
  },

  keepItemLinksOnCopy: ["classAssociations"],
};

/**
 * Non-PF1 config entries
 *
 * These keys are merged into {@link CONFIG} using {@link mergeObject}.
 */
export const CONFIG_OVERRIDES = {
  "Combat.initiative.decimals": 2,
  "debug.roll": false,
  "debug.template": false,
};
