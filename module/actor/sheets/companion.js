import { ActorSheetPF } from "./base.js";
import { LevelUpForm } from "../../apps/level-up.js";
import { CR } from "../../lib.js";
import { customRolls } from "../../sidebar/chat-message.js";

/**
 * An Actor sheet for companion type actors in the PF system.
 * Extends the base ActorSheetPF class.
 *
 * @type {ActorSheetPF}
 */
export class ActorSheetPFCompanion extends ActorSheetPF {
  /**
   * Define default rendering options for the NPC sheet
   *
   * @returns {Object}
   */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["pf1", "sheet", "actor", "companion"],
      width: 800,
      height: 840,
    });
  }

  /* -------------------------------------------- */
  /*  Rendering                                   */
  /* -------------------------------------------- */

  /**
   * Get the correct HTML template path to use for rendering this particular sheet
   *
   * @type {string}
   */
  get template() {
    if (!game.user.isGM && this.actor.limited) return "systems/pf1/templates/actors/limited-sheet.hbs";
    return "systems/pf1/templates/actors/companion-sheet.hbs";
  }

  /* -------------------------------------------- */

  /**
   * Add some extra data when rendering the sheet to reduce the amount of logic required within the template.
   */
  async getData() {
    const data = await super.getData();

    // BAB iteratives
    const bab = data.data.attributes.bab.total;
    let iters = [bab];
    for (let i = bab - 5; i > 0; i -= 5) iters.push(i);
    data["iteratives"] = `+${iters.join(" / +")}`;

    // Challenge Rating
    try {
      data.labels.cr = CR.fromNumber(getProperty(this.actor.data, "data.details.cr.base"));
    } catch (e) {
      try {
        data.labels.cr = CR.fromNumber(getProperty(this.actor.data, "data.details.cr"));
      } catch (e) {
        data.labels.cr = CR.fromNumber(1);
      }
    }
    
    // Return data for rendering
    return data;
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers
  /* -------------------------------------------- */

  /**
   * Activate event listeners using the prepared sheet HTML
   *
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   */
  activateListeners(html) {
    super.activateListeners(html);
    if (!this.options.editable) return;

    // Inventory Functions
    html.find(".currency-convert").click(this._onConvertCurrency.bind(this));
  
    // Adjust CR
    html.find("span.text-box.cr-input").on("click", (event) => {
      this._onSpanTextInput(event, this._adjustCR.bind(this));
    });

    // Death roll
    html.find(".hp .rollable").click(this._onRollDeath.bind(this));
  }

  _adjustCR(event) {
    event.preventDefault();
    const el = event.currentTarget;

    const value = CR.fromString(el.tagName.toUpperCase() === "INPUT" ? el.value : el.innerText);
    const name = el.getAttribute("name");
    if (name) {
      this._pendingUpdates[name] = value;
    }

    // Update on lose focus
    if (event.originalEvent instanceof MouseEvent) {
      if (!this._submitQueued) {
        $(el).one("mouseleave", (event) => {
          this._onSubmit(event);
        });
      }
    } else this._onSubmit(event);
  }

  _onLevelUp(event) {
    event.preventDefault;
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.items.get(itemId);

    const app = Object.values(this.actor.apps).find((o) => {
      return o instanceof LevelUpForm && o._element;
    });
    if (app) app.bringToTop();
    else new LevelUpForm(item).render(true);
  }

  _onRollDeath(event) {
    event.preventDefault();

    let rollData = this.actor.getRollData();

    const content = `/d ${rollData.attributes.hp.max}[${game.i18n.localize("PF1.Dying")}]`;
    const speaker = CONFIG.ChatMessage.documentClass.getSpeaker();
    
    customRolls(content, speaker, rollData);
  }

  /* -------------------------------------------- */

  async _onConvertCurrency(event) {
    event.preventDefault();
    const curr = duplicate(this.actor.data.data.currency);
    const convert = {
      cp: { into: "sp", each: 10 },
      sp: { into: "gp", each: 10 },
      gp: { into: "pp", each: 10 },
    };
    for (let [c, t] of Object.entries(convert)) {
      let change = Math.floor(curr[c] / t.each);
      curr[c] -= change * t.each;
      curr[t.into] += change;
    }
    return this.actor.update({ "data.currency": curr });
  }
}
