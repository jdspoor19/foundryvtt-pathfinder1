import { ActorSheetPF } from "./base.js";
import { LevelUpForm } from "../../apps/level-up.js";
import { DicePF } from "../../dice.js";

/**
 * An Actor sheet for player character type actors in the PF system.
 * Extends the base ActorSheetPF class.
 *
 * @type {ActorSheetPF}
 */
export class ActorSheetPFCharacter extends ActorSheetPF {
  /**
   * Define default rendering options for the NPC sheet
   *
   * @returns {Object}
   */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["pf1", "sheet", "actor", "character"],
      width: 800,
      height: 840,
    });
  }

  /* -------------------------------------------- */
  /*  Rendering                                   */
  /* -------------------------------------------- */

  /**
   * Get the correct HTML template path to use for rendering this particular sheet
   *
   * @type {string}
   */
  get template() {
    if (!game.user.isGM && this.actor.limited) return "systems/pf1/templates/actors/limited-sheet.hbs";
    return "systems/pf1/templates/actors/character-sheet.hbs";
  }

  /* -------------------------------------------- */

  /**
   * Add some extra data when rendering the sheet to reduce the amount of logic required within the template.
   */
  async getData() {
    const data = await super.getData();

    // BAB iteratives
    const bab = data.data.attributes.bab.total;
    let iters = [bab];
    for (let i = bab - 5; i > 0; i -= 5) iters.push(i);
    data["iteratives"] = `+${iters.join(" / +")}`;

    // Return data for rendering
    return data;
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers
  /* -------------------------------------------- */

  /**
   * Activate event listeners using the prepared sheet HTML
   *
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   */
  activateListeners(html) {
    super.activateListeners(html);
    if (!this.options.editable) return;

    // Inventory Functions
    html.find(".currency-convert").click(this._onConvertCurrency.bind(this));

    // Death and Dying roll
    html.find(".hp .rollable").click(this._onRollDeathDying.bind(this));
  }

  _onRollDeathDying(event) {
    event.preventDefault();

    let rollData = this.actor.getRollData();
    const parts = [];
    const props = [];

    const describePart = (value, label) => parts.push(`${value}[${label}]`);
    describePart(rollData.abilities.sta.mod + rollData.abilities.sta.checkMod, game.i18n.localize("PF1.AbilityStaMod"));
    describePart(rollData.attributes.hp.value, game.i18n.localize("PF1.Health"));

    return DicePF.d20Roll({
      actor: this.actor,
      event: event,
      parts,
      dice: "1d20",
      data: rollData,
      title: game.i18n.localize("PF1.Dying"),
      speaker: ChatMessage.getSpeaker({ actor: this.actor }),
      takeTwenty: false,
      chatTemplate: "systems/pf1/templates/chat/roll-ext.hbs",
      chatTemplateData: { hasProperties: props.length > 0, properties: props },
      chatMessage: true,
      noSound: false,
    });
  }

  _onLevelUp(event) {
    event.preventDefault;
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.items.get(itemId);

    const app = Object.values(this.actor.apps).find((o) => {
      return o instanceof LevelUpForm && o._element;
    });
    if (app) app.render(true, { focus: true });
    else new LevelUpForm(item).render(true);
  }

  /* -------------------------------------------- */

  async _onConvertCurrency(event) {
    event.preventDefault();
    const curr = duplicate(this.actor.data.data.currency);
    const convert = {
      cp: { into: "sp", each: 10 },
      sp: { into: "gp", each: 10 },
      gp: { into: "pp", each: 10 },
    };
    for (const [c, t] of Object.entries(convert)) {
      const change = Math.floor(curr[c] / t.each);
      curr[c] -= change * t.each;
      curr[t.into] += change;
    }
    return this.actor.update({ "data.currency": curr });
  }
}
