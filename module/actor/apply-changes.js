import { PF1 } from "../config";
import { fractionalToString } from "../lib";

/**
 *
 */
export function applyChanges() {
  this.changeOverrides = {};
  const c = Array.from(this.changes);

  const priority = getSortChangePriority.call(this);
  const _sortChanges = function (a, b) {
    const typeA = priority.types.indexOf(a.subTarget);
    const typeB = priority.types.indexOf(b.subTarget);
    const modA = priority.modifiers.indexOf(a.modifier);
    const modB = priority.modifiers.indexOf(b.modifier);
    let prioA = typeof a.priority === "string" ? parseInt(a.priority) : a.priority;
    let prioB = typeof b.priority === "string" ? parseInt(b.priority) : b.priority;
    prioA = (prioA || 0) + 1000;
    prioB = (prioB || 0) + 1000;
    if (a.data && a.data.operator == "set") {
      prioA += 1000
    }
    if (b.data && b.data.operator == "set") {
      prioB += 1000
    }
  
    const result = prioB - prioA || typeA - typeB || modA - modB;
    return result
  };

  // Organize changes by priority
  c.sort((a, b) => _sortChanges.call(this, a, b));

  // Parse change flags
  for (const i of this.changeItems) {
    for (const [k, v] of Object.entries(i.data.data.changeFlags)) {
      if (v === true) {
        this.flags[k] = true;

        if (k === "loseDexToAC") {
          for (const k2 of ["normal", "touch"]) {
            getSourceInfo(this.sourceInfo, `data.attributes.ac.${k2}.total`).negative.push({
              value: game.i18n.localize("PF1.ChangeFlagLoseDexToAC"),
              name: i.name,
              type: i.type,
            });
          }
          // getSourceInfo(this.sourceInfo, "data.attributes.cmd.total").negative.push({
          //   value: game.i18n.localize("PF1.ChangeFlagLoseDexToAC"),
          //   name: i.name,
          //   type: i.type,
          // });
        }
      }
    }
  }
  this.refreshDerivedData();

  // Determine continuous changes
  const continuousChanges = c.filter((o) => o.continuous === true);

  // Apply all changes
  for (const change of c) {
    let flats = getChangeFlat.call(this, change.subTarget, change.modifier);
    if (!(flats instanceof Array)) flats = [flats];
    for (const f of flats) {
      if (!this.changeOverrides[f]) this.changeOverrides[f] = createOverride();
    }

    change.applyChange(this, flats, this.flags);

    // Apply continuous changes
    for (const cc of continuousChanges) {
      if (cc === change) continue;

      let flats = getChangeFlat.call(this, cc.subTarget, cc.modifier);
      if (!(flats instanceof Array)) flats = [flats];
      for (const f of flats) {
        if (!this.changeOverrides[f]) this.changeOverrides[f] = createOverride();
      }

      cc.applyChange(this, flats, this.flags);
    }

    this.refreshDerivedData();
  }

  resetSkills.call(this);

  this.changeOverrides = expandObject(this.changeOverrides);
}

const createOverride = function () {
  const result = {
    add: {},
    set: {},
  };

  for (const k of Object.keys(CONFIG.PF1.bonusModifiers)) {
    result.add[k] = null;
    result.set[k] = null;
  }

  return result;
};

const getSortChangePriority = function () {
  /** @type {[string, {sort: number}][]}*/
  const skillTargets = this._skillTargets.map((target, index) => [target, { sort: 76000 + index * 10 }]);
  const buffTargets = Object.entries(PF1.buffTargets);
  const types = [...skillTargets, ...buffTargets]
    .sort(([, { sort: aSort }], [, { sort: bSort }]) => aSort - bSort)
    .map(([target]) => target);

  return {
    types: types,
    modifiers: [
      "untyped",
      "untypedPerm",
      "base",
      "temp",
      "enh",
      "natural",
      "dodge",
      "inherent",
      "deflection",
      "morale",
      "luck",
      "sacred",
      "insight",
      "resist",
      "profane",
      "trait",
      "racial",
      "size",
      "competence",
      "circumstance",
      "alchemical",
      "penalty",
    ],
  };
};

export const getChangeFlat = function (changeTarget, changeType, curData = null) {
  if (changeTarget == null) return null;

  curData = curData ?? this.data.data;
  const result = [];

  switch (changeTarget) {
    case "mhp":
      return "data.attributes.hp.max";
    case "resolve":
      return "data.attributes.resolve.max";
    case "str":
    case "dex":
    case "sta":
    case "int":
    case "wis":
    case "mna":
    case "prs":
    case "cnv":
    case "fth":
      if (["base", "untypedPerm", "racial"].includes(changeType))
        return [`data.abilities.${changeTarget}.total`, `data.abilities.${changeTarget}.base`];
      return `data.abilities.${changeTarget}.total`;
    case "strMod":
    case "dexMod":
    case "staMod":
    case "intMod":
    case "wisMod":
    case "mnaMod":
    case "prsMod":
    case "cnvMod":
    case "fthMod":
      return `data.abilities.${changeTarget.slice(0, 3)}.mod`;
    case "bdy":
    case "mnd":
    case "spr":
      return `data.aspects.${changeTarget}.mod`;
    case "artificerCreations":
      return "data.attributes.feats.artificerCreations";
    case "channeling":
      return "data.attributes.feats.channeling";
    case "eldritchBlast":
      return "data.attributes.feats.eldritchBlast";
    case "grenadier":
      return "data.attributes.feats.grenadier";
    case "inspiration":
      return "data.attributes.feats.inspiration";
    case "monastic":
      return "data.attributes.feats.monastic";
    case "rage":
      return "data.attributes.feats.rage";
    case "suddenStrike":
      return "data.attributes.feats.suddenStrike";
    case "occultTier":
      return "data.attributes.spells.spellbooks.occult.tier";
    case "arcaneTier":
      return "data.attributes.spells.spellbooks.arcane.tier";
    case "arcanePool":
      return "data.attributes.spells.spellbooks.arcane.pool.formula";
    case "divineTier":
      return "data.attributes.spells.spellbooks.divine.tier";
    case "ac":
      switch (changeType) {
        case "natural":
          return [
            "data.attributes.ac.normal.total",
            "data.attributes.ac.flatFooted.total",
            "data.attributes.cmd.total",
          ];
        case "dodge":
        case "deflection":
          if (this.flags["loseDexToAC"]) return [];
          return ["data.attributes.ac.normal.total", "data.attributes.ac.touch.total", "data.attributes.cmd.total"];
        case "circumstance":
        case "luck":
          return [
            "data.attributes.ac.normal.total",
            "data.attributes.ac.touch.total",
            "data.attributes.ac.flatFooted.total",
            "data.attributes.cmd.total",
          ];
        default:
          return [
            "data.attributes.ac.normal.total",
            "data.attributes.ac.touch.total",
            "data.attributes.ac.flatFooted.total",
          ];
      }
    case "aac": {
      const targets = ["data.ac.normal.total"];
      switch (changeType) {
        case "base":
          targets.push("data.ac.normal.base");
          break;
        case "enh":
          targets.push("data.ac.normal.enh");
          break;
        default:
          targets.push("data.ac.normal.misc");
          break;
      }
      return targets;
    }
    case "sac": {
      const targets = ["data.ac.shield.total"];
      switch (changeType) {
        case "base":
          targets.push("data.ac.shield.base");
          break;
        case "enh":
          targets.push("data.ac.shield.enh");
          break;
        default:
          targets.push("data.ac.shield.misc");
          break;
      }
      return targets;
    }
    case "nac": {
      const targets = ["data.ac.natural.total"];
      switch (changeType) {
        case "base":
          targets.push("data.ac.natural.base");
          break;
        case "enh":
          targets.push("data.ac.natural.enh");
          break;
        default:
          targets.push("data.ac.natural.misc");
          break;
      }
      return targets;
    }
    case "tac":
      return "data.attributes.ac.touch.total";
    case "ffac":
      return "data.attributes.ac.flatFooted.total";
    case "bab":
      return "data.attributes.bab.total";
    case "~attackCore":
      return "data.attributes.attack.shared";
    case "attack":
      return "data.attributes.attack.general";
    case "mattack":
      return "data.attributes.attack.melee";
    case "rattack":
      return "data.attributes.attack.ranged";
    case "critConfirm":
      return "data.attributes.attack.critConfirm";
    case "allSavingThrows":
      return [
        "data.attributes.savingThrows.bdySave.total",
        "data.attributes.savingThrows.mndSave.total",
        "data.attributes.savingThrows.sprSave.total",
      ];
    case "bdySave":
      return "data.attributes.savingThrows.bdySave.total";
    case "mndSave":
      return "data.attributes.savingThrows.mndSave.total";
    case "sprSave":
      return "data.attributes.savingThrows.sprSave.total";
    case "skills":
      for (const [a, skl] of Object.entries(curData.skills)) {
        if (skl == null) continue;
        result.push(`data.skills.${a}.changeBonus`);
      }
      return result;
    case "bdySkills":
        for (let [a, skl] of Object.entries(curData.skills)) {
          if (skl == null) continue;
          if (skl.ability === "bdy") result.push(`data.skills.${a}.changeBonus`);
        }
        return result;
    case "strSkills":
      for (const [a, skl] of Object.entries(curData.skills)) {
        if (skl == null) continue;
        if (skl.ability === "str") result.push(`data.skills.${a}.changeBonus`);
      }
      return result;
    case "dexSkills":
      for (const [a, skl] of Object.entries(curData.skills)) {
        if (skl == null) continue;
        if (skl.ability === "dex") result.push(`data.skills.${a}.changeBonus`);
      }
      return result;
    case "staSkills":
      for (let [a, skl] of Object.entries(curData.skills)) {
        if (skl == null) continue;
        if (skl.ability === "sta") result.push(`data.skills.${a}.changeBonus`);
      }
      return result;
    case "mndSkills":
      for (let [a, skl] of Object.entries(curData.skills)) {
        if (skl == null) continue;
        if (skl.ability === "mnd") result.push(`data.skills.${a}.changeBonus`);
      }
      return result;
    case "intSkills":
      for (const [a, skl] of Object.entries(curData.skills)) {
        if (skl == null) continue;
        if (skl.ability === "int") result.push(`data.skills.${a}.changeBonus`);
      }
      return result;
    case "wisSkills":
      for (const [a, skl] of Object.entries(curData.skills)) {
        if (skl == null) continue;
        if (skl.ability === "wis") result.push(`data.skills.${a}.changeBonus`);
      }
      return result;
    case "mnaSkills":
      for (let [a, skl] of Object.entries(curData.skills)) {
        if (skl == null) continue;
        if (skl.ability === "mna") result.push(`data.skills.${a}.changeBonus`);
      }
      return result;
    case "sprSkills":
      for (let [a, skl] of Object.entries(curData.skills)) {
        if (skl == null) continue;
        if (skl.ability === "spr") result.push(`data.skills.${a}.changeBonus`);
      }
      return result;
    case "prsSkills":
      for (let [a, skl] of Object.entries(curData.skills)) {
        if (skl == null) continue;
        if (skl.ability === "prs") result.push(`data.skills.${a}.changeBonus`);
      }
      return result;
    case "cnvSkills":
      for (let [a, skl] of Object.entries(curData.skills)) {
        if (skl == null) continue;
        if (skl.ability === "cnv") result.push(`data.skills.${a}.changeBonus`);
      }
      return result;
    case "fthSkills":
      for (let [a, skl] of Object.entries(curData.skills)) {
        if (skl == null) continue;
        if (skl.ability === "fth") result.push(`data.skills.${a}.changeBonus`);
      }
      return result;
    case "allChecks":
      return [
        "data.abilities.str.checkMod",
        "data.abilities.dex.checkMod",
        "data.abilities.sta.checkMod",
        "data.abilities.int.checkMod",
        "data.abilities.wis.checkMod",
        "data.abilities.mna.checkMod",
        "data.abilities.prs.checkMod",
        "data.abilities.cnv.checkMod",
        "data.abilities.fth.checkMod",
      ];
    case "strChecks":
      return "data.abilities.str.checkMod";
    case "dexChecks":
      return "data.abilities.dex.checkMod";
    case "staChecks":
      return "data.abilities.sta.checkMod";
    case "intChecks":
      return "data.abilities.int.checkMod";
    case "wisChecks":
      return "data.abilities.wis.checkMod";
    case "mnaChecks":
      return "data.abilities.mna.checkMod";
    case "prsChecks":
      return "data.abilities.prs.checkMod";
    case "cnvChecks":
      return "data.abilities.cnv.checkMod";
    case "fthChecks":
      return "data.abilities.fth.checkMod";
    case "concentration":
      return ["data.attributes.spells.spellbooks.divine.concentration.formula", "data.attributes.spells.spellbooks.arcane.concentration.formula"]
    case "allSpeeds":
      if (changeType === "base") return [
        "data.attributes.speed.land.base",
        "data.attributes.speed.climb.base",
        "data.attributes.speed.swim.base",
        "data.attributes.speed.burrow.base",
        "data.attributes.speed.fly.base",
        "data.attributes.speed.land.total",
        "data.attributes.speed.climb.total",
        "data.attributes.speed.swim.total",
        "data.attributes.speed.burrow.total",
        "data.attributes.speed.fly.total",
      ];
      return [
        "data.attributes.speed.land.total",
        "data.attributes.speed.climb.total",
        "data.attributes.speed.swim.total",
        "data.attributes.speed.burrow.total",
        "data.attributes.speed.fly.total",
      ];
    case "landSpeed":
      if (changeType === "base") return ["data.attributes.speed.land.base", "data.attributes.speed.land.total"];
      return "data.attributes.speed.land.total";
    case "climbSpeed":
      if (changeType === "base") return ["data.attributes.speed.climb.base", "data.attributes.speed.climb.total"];
      return "data.attributes.speed.climb.total";
    case "swimSpeed":
      if (changeType === "base") return ["data.attributes.speed.swim.base", "data.attributes.speed.swim.total"];
      return "data.attributes.speed.swim.total";
    case "burrowSpeed":
      if (changeType === "base") return ["data.attributes.speed.burrow.base", "data.attributes.speed.burrow.total"];
      return "data.attributes.speed.burrow.total";
    case "flySpeed":
      if (changeType === "base") return ["data.attributes.speed.fly.base", "data.attributes.speed.fly.total"];
      return "data.attributes.speed.fly.total";
    case "cmb":
      return "data.attributes.cmb.total";
    case "cmd":
      return "data.attributes.cmd.total";
    case "init":
      return "data.attributes.init.total";
    case "acpA":
      return "data.attributes.acp.armorBonus";
    case "acpS":
      return "data.attributes.acp.shieldBonus";
    case "mDexA":
      return "data.attributes.mDex.armorBonus";
    case "mDexS":
      return "data.attributes.mDex.shieldBonus";
    case "spellResist":
      return "data.attributes.sr.total";
    case "bonusCarryCapacity":
      return "data.details.bonusCarryCapacityFormula";
  }

  if (changeTarget.match(/^skill\.([a-zA-Z0-9]+)$/)) {
    const sklKey = RegExp.$1;
    if (curData.skills[sklKey] != null) {
      return `data.skills.${sklKey}.changeBonus`;
    }
  }

  // Try to determine a change flat from hooks
  {
    const result = { keys: [] };
    Hooks.callAll("pf1.getChangeFlat", changeTarget, changeType, result);
    if (result.keys && result.keys.length) return result.keys;
  }
  return null;
};

const getNegativeEnergyDrain = function (d) {
  return -d.attributes.energyDrain;
};

const getAbilityMod = function (ability) {
  return function (d) {
    return d.abilities[ability]?.mod ?? 0;
  };
};

export const addDefaultChanges = function (changes) {
  const actorData = this.data.data;
  // Call hook
  const tempChanges = [];
  Hooks.callAll("pf1.addDefaultChanges", this, tempChanges);
  changes.push(...tempChanges.filter((c) => c instanceof game.pf1.documentComponents.ItemChange));

  // Add movement speed(s)
  for (const [k, s] of Object.entries(actorData.attributes.speed)) {
    let base = s.base;
    if (!base) base = 0;
    let total = base;
    if (total > 0) {
      if (k == "land") {
        total += Math.floor(this.data.data.abilities.dex.mod / 2) * 5;
      }
    }
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: total,
        target: "speed",
        subTarget: `${k}Speed`,
        modifier: "base",
        operator: "set",
        priority: 1001,
      })
    );
    if (total > 0) {
      getSourceInfo(this.sourceInfo, `data.attributes.speed.${k}.base`).positive.push({
        value: total,
        name: game.i18n.localize("PF1.Base"),
      });
    }
  }

  // Add base attack modifiers shared by all attacks
  {
    // ACP to attack
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: (d) => -d.attributes.acp.attackPenalty,
        operator: "function",
        target: "attack",
        subTarget: "~attackCore",
        modifier: "penalty",
      })
    );
    getSourceInfo(this.sourceInfo, "data.attributes.attack.shared").negative.push({
      formula: "-@attributes.acp.attackPenalty",
      name: game.i18n.localize("PF1.ArmorCheckPenalty"),
    });
    // ACP to skills
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: (d) => -d.attributes.acp.attackPenalty,
        operator: "function",
        target: "skills",
        subTarget: "bdySkills",
        modifier: "penalty",
      })
    );
    for (let f of getChangeFlat.call(this, "bdySkills", "penalty")) {
      getSourceInfo(this.sourceInfo, f).negative.push({
        formula: "-@attributes.acp.attackPenalty",
        name: game.i18n.localize("PF1.ArmorCheckPenalty"),
      });
    }
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: "-@attributes.acp.attackPenalty",
        target: "skills",
        subTarget: "strSkills",
        modifier: "penalty",
      })
    );
    for (let f of getChangeFlat.call(this, "strSkills", "penalty")) {
      getSourceInfo(this.sourceInfo, f).negative.push({
        formula: "-@attributes.acp.attackPenalty",
        name: game.i18n.localize("PF1.ArmorCheckPenalty"),
      });
    }
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: "-@attributes.acp.attackPenalty",
        target: "skills",
        subTarget: "dexSkills",
        modifier: "penalty",
      })
    );
    for (let f of getChangeFlat.call(this, "dexSkills", "penalty")) {
      getSourceInfo(this.sourceInfo, f).negative.push({
        formula: "-@attributes.acp.attackPenalty",
        name: game.i18n.localize("PF1.ArmorCheckPenalty"),
      });
    }
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: "-@attributes.acp.attackPenalty",
        target: "skills",
        subTarget: "staSkills",
        modifier: "penalty",
      })
    );
    for (let f of getChangeFlat.call(this, "staSkills", "penalty")) {
      getSourceInfo(this.sourceInfo, f).negative.push({
        formula: "-@attributes.acp.attackPenalty",
        name: game.i18n.localize("PF1.ArmorCheckPenalty"),
      });
    }
  }

  // Add variables to Combat Maneuvers
  {
    // Bonus to CMB
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: "@attributes.cmb.bonus",
        target: "misc",
        subTarget: "cmb",
        modifier: "untypedPerm",
      })
    );
    getSourceInfo(this.sourceInfo, "data.attributes.cmb.total").positive.push({
      formula: "@attributes.cmb.bonus",
      name: game.i18n.localize("PF1.Bonus"),
    });
    // BAB to CMB
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: "@attributes.bab.total",
        target: "misc",
        subTarget: "cmb",
        modifier: "untypedPerm",
      })
    );
    getSourceInfo(this.sourceInfo, "data.attributes.cmb.total").positive.push({
      formula: "@attributes.bab.total",
      name: game.i18n.localize("PF1.BAB"),
    });
    // Aspect to CMB
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: `@aspects.bdy.mod`,
        target: "misc",
        subTarget: "cmb",
        modifier: "untypedPerm",
      })
    );
    getSourceInfo(this.sourceInfo, "data.attributes.cmb.total").positive.push({
      formula: "@aspects.bdy.mod",
      name: CONFIG.PF1.aspects["bdy"],
    });

    // Bonus to CMD
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: "@attributes.cmd.bonus",
        target: "misc",
        subTarget: "cmd",
        modifier: "untypedPerm",
      })
    );
    for (const k of ["total", "flatFootedTotal"]) {
      getSourceInfo(this.sourceInfo, `data.attributes.cmd.${k}`).positive.push({
        formula: "@attributes.cmd.bonus",
        name: game.i18n.localize("PF1.Bonus"),
      });
    }
    // BAB to CMD
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: "@attributes.bab.total",
        target: "misc",
        subTarget: "cmd",
        modifier: "untypedPerm",
      })
    );
    for (const k of ["total", "flatFootedTotal"]) {
      getSourceInfo(this.sourceInfo, `data.attributes.cmd.${k}`).positive.push({
        formula: "@attributes.bab.total",
        name: game.i18n.localize("PF1.BAB"),
      });
    }
    // Body Aspect to CMD
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: "@aspects.bdy.mod",
        target: "misc",
        subTarget: "cmd",
        modifier: "untypedPerm",
      })
    );
    for (const k of ["total", "flatFootedTotal"]) {
      getSourceInfo(this.sourceInfo, `data.attributes.cmd.${k}`).positive.push({
        formula: "@aspects.bdy.mod",
        name: CONFIG.PF1.aspects["bdy"],
      });
    }
    // Stamina to CMD
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: "@abilities.sta.mod",
        target: "misc",
        subTarget: "cmd",
        modifier: "untypedPerm",
      })
    );
    for (const k of ["total", "flatFootedTotal"]) {
      getSourceInfo(this.sourceInfo, `data.attributes.cmd.${k}`).positive.push({
        formula: "@abilities.sta.mod",
        name: CONFIG.PF1.abilities["sta"],
      });
    }
  }

  // Add Modifiers to Initiative
  {
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: "@attributes.init.bonus",
        target: "misc",
        subTarget: "init",
        modifier: "untypedPerm",
      })
    );
    getSourceInfo(this.sourceInfo, "data.attributes.init.total").positive.push({
      formula: "@attributes.init.bonus",
      name: game.i18n.localize("PF1.Bonus"),
    });

    const abls = getProperty(this.data, "data.attributes.init.abilities");
    abls.forEach((abl) => {
      changes.push(
        game.pf1.documentComponents.ItemChange.create({
          formula: getAbilityMod(abl),
          operator: "function",
          target: "misc",
          subTarget: "init",
          modifier: "untypedPerm",
          priority: -100,
        })
      );
      getSourceInfo(this.sourceInfo, "data.attributes.init.total").positive.push({
        formula: `@abilities.${abl}.mod`,
        name: CONFIG.PF1.abilities[abl],
      });
    });
  }

  // Spell Resistance
  {
    const sr = actorData.attributes.sr.formula || 0;
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: sr,
        target: "misc",
        subTarget: "spellResist",
        modifier: "base",
        priority: 1000,
      })
    );
    getSourceInfo(this.sourceInfo, "data.attributes.sr.total").positive.push({
      formula: sr,
      name: game.i18n.localize("PF1.Base"),
    });
  }
  {
    // Carry capacity strength bonus
    const cStr = actorData.details.carryCapacity.bonus.user || 0;
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: cStr,
        target: "misc",
        subTarget: "carryStr",
        modifier: "untyped",
        priority: 1000,
      })
    );
    getSourceInfo(this.sourceInfo, "data.details.carryCapacity.bonus.total").positive.push({
      formula: cStr.toString(),
      name: game.i18n.localize("PF1.Custom"),
    });
    // Carry capacity multiplier
    const cMultBase = actorData.details.carryCapacity.multiplier.base ?? 1;
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: cMultBase,
        target: "misc",
        subTarget: "carryMult",
        modifier: "base",
        priority: 1000,
      })
    );
    getSourceInfo(this.sourceInfo, "data.details.carryCapacity.multiplier.total").positive.push({
      formula: cMultBase.toString(),
      name: game.i18n.localize("PF1.Base"),
    });
    const cMult = actorData.details.carryCapacity.multiplier.user || 0;
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: cMult,
        target: "misc",
        subTarget: "carryMult",
        modifier: "untyped",
        priority: 1000,
      })
    );
    getSourceInfo(this.sourceInfo, "data.details.carryCapacity.multiplier.total").positive.push({
      formula: cMult.toString(),
      name: game.i18n.localize("PF1.Custom"),
    });
  }

  // Add armor bonuses from equipment
  this.data.items
    .filter((obj) => {
      return obj.type === "equipment" && obj.data.data.equipped;
    })
    .forEach((item) => {
      let armorTarget = "aac";
      if (item.data.data.equipmentType === "shield") armorTarget = "sac";
      // Push base armor
      if (item.data.data.armor.value) {
        let ac = item.data.data.armor.value;
        if (item.data.data.broken) ac = Math.floor(ac / 2);
        ac += item.data.data.armor.enh;
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: ac,
            target: "ac",
            subTarget: armorTarget,
            modifier: "base",
          })
        );
        for (const k of ["normal", "flatFooted"]) {
          getSourceInfo(this.sourceInfo, `data.attributes.ac.${k}.total`).positive.push({
            value: ac,
            name: item.name,
            type: item.type,
          });
        }
      }
    });

  // Add size bonuses to various attributes
  const sizeKey = actorData.traits.size;
  if (sizeKey !== "med") {
    // AC
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: CONFIG.PF1.sizeMods[sizeKey],
        target: "ac",
        subTarget: "ac",
        modifier: "size",
      })
    );
    for (const k of ["normal", "touch", "flatFooted"]) {
      getSourceInfo(this.sourceInfo, `data.attributes.ac.${k}.total`).positive.push({
        value: CONFIG.PF1.sizeMods[sizeKey],
        type: "size",
      });
    }

    // Attack
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: CONFIG.PF1.sizeMods[sizeKey].toString(),
        target: "attack",
        subTarget: "~attackCore",
        modifier: "size",
      })
    );
    getSourceInfo(this.sourceInfo, "data.attributes.attack.shared").positive.push({
      value: CONFIG.PF1.sizeMods[sizeKey],
      type: "size",
    });
    // CMB
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: CONFIG.PF1.sizeSpecialMods[sizeKey].toString(),
        target: "misc",
        subTarget: "cmb",
        modifier: "size",
      })
    );
    getSourceInfo(this.sourceInfo, "data.attributes.cmb.total").negative.push({
      value: CONFIG.PF1.sizeSpecialMods[sizeKey],
      type: "size",
    });
    // CMD
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: CONFIG.PF1.sizeSpecialMods[sizeKey],
        target: "misc",
        subTarget: "cmd",
        modifier: "size",
      })
    );
    for (const k of ["total", "flatFootedTotal"]) {
      getSourceInfo(this.sourceInfo, `data.attributes.cmd.${k}`).positive.push({
        value: CONFIG.PF1.sizeSpecialMods[sizeKey],
        type: "size",
      });
    }
  }

  // Add conditions
  for (const [con, v] of Object.entries(actorData.attributes.conditions || {})) {
    if (!v) continue;

    switch (con) {
      case "pf1_blind":
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "defense",
            subTarget: "ac",
            modifier: "penalty",
          })
        );
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "misc",
            subTarget: "cmd",
            modifier: "penalty",
          })
        );

        for (const k of [
          "data.attributes.ac.normal.total",
          "data.attributes.ac.touch.total",
          "data.attributes.ac.flatFooted.total",
          "data.attributes.cmd.total",
          "data.attributes.cmd.flatFootedTotal",
        ]) {
          getSourceInfo(this.sourceInfo, k).negative.push({
            value: -2,
            name: game.i18n.localize("PF1.CondBlind"),
          });
        }
        if (!this.flags["loseDexToAC"]) {
          for (const k of [
            "data.attributes.ac.normal.total",
            "data.attributes.ac.touch.total",
            "data.attributes.cmd.total",
          ]) {
            getSourceInfo(this.sourceInfo, k).negative.push({
              value: game.i18n.localize("PF1.ChangeFlagLoseDexToAC"),
              name: game.i18n.localize("PF1.CondBlind"),
            });
          }
          this.flags["loseDexToAC"] = true;
        }
        break;
      case "cowering":
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: "-2",
            target: "defense",
            subTarget: "ac",
            modifier: "penalty",
          })
        );
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "misc",
            subTarget: "cmd",
            modifier: "penalty",
          })
        );

        for (const k of [
          "data.attributes.ac.normal.total",
          "data.attributes.ac.touch.total",
          "data.attributes.ac.flatFooted.total",
          "data.attributes.cmd.total",
          "data.attributes.cmd.flatFootedTotal",
        ]) {
          getSourceInfo(this.sourceInfo, k).negative.push({
            value: -2,
            name: game.i18n.localize("PF1.CondCowering"),
          });
        }
        if (!this.flags["loseDexToAC"]) {
          for (const k of [
            "data.attributes.ac.normal.total",
            "data.attributes.ac.touch.total",
            "data.attributes.cmd.total",
          ]) {
            getSourceInfo(this.sourceInfo, k).negative.push({
              value: game.i18n.localize("PF1.ChangeFlagLoseDexToAC"),
              name: game.i18n.localize("PF1.CondCowering"),
            });
          }
          this.flags["loseDexToAC"] = true;
        }
        break;
      case "dazzled":
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -1,
            target: "attack",
            subTarget: "attack",
            modifier: "penalty",
          })
        );
        getSourceInfo(this.sourceInfo, "data.attributes.attack.general").negative.push({
          value: -1,
          name: game.i18n.localize("PF1.CondDazzled"),
        });
        break;
      case "pf1_deaf":
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -4,
            target: "misc",
            subTarget: "init",
            modifier: "penalty",
          })
        );
        getSourceInfo(this.sourceInfo, "data.attributes.init.total").negative.push({
          value: -4,
          name: game.i18n.localize("PF1.CondDeaf"),
        });
        break;
      case "entangled":
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "defense",
            subTarget: "ac",
            modifier: "penalty",
          })
        );
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "misc",
            subTarget: "cmd",
            modifier: "penalty",
          })
        );

        for (const k of [
          "data.attributes.ac.normal.total",
          "data.attributes.ac.touch.total",
          "data.attributes.ac.flatFooted.total",
          "data.attributes.cmd.total",
          "data.attributes.cmd.flatFootedTotal",
        ]) {
          getSourceInfo(this.sourceInfo, k).negative.push({
            value: -2,
            name: game.i18n.localize("PF1.CondEntangled"),
          });
        }

        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "attack",
            subTarget: "attack",
            modifier: "penalty",
            flavor: game.i18n.localize("PF1.CondEntangled"),
          })
        );
        getSourceInfo(this.sourceInfo, "data.attributes.attack.general").negative.push({
          value: -2,
          name: game.i18n.localize("PF1.CondEntangled"),
        });
        break;
      case "grappled":
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "defense",
            subTarget: "ac",
            modifier: "penalty",
          })
        );
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "misc",
            subTarget: "cmd",
            modifier: "penalty",
          })
        );

        for (const k of [
          "data.attributes.ac.normal.total",
          "data.attributes.ac.touch.total",
          "data.attributes.ac.flatFooted.total",
          "data.attributes.cmd.total",
          "data.attributes.cmd.flatFootedTotal",
        ]) {
          getSourceInfo(this.sourceInfo, k).negative.push({
            value: -2,
            name: game.i18n.localize("PF1.CondGrappled"),
          });
        }

        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "attack",
            subTarget: "attack",
            modifier: "penalty",
            flavor: game.i18n.localize("PF1.CondGrappled"),
          })
        );
        getSourceInfo(this.sourceInfo, "data.attributes.attack.general").negative.push({
          value: -2,
          name: game.i18n.localize("PF1.CondGrappled"),
        });
        break;
      case "helpless":
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -8,
            target: "defense",
            subTarget: "ac",
            modifier: "penalty",
          })
        );
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -8,
            target: "misc",
            subTarget: "cmd",
            modifier: "penalty",
          })
        );

        for (const k of [
          "data.attributes.ac.normal.total",
          "data.attributes.ac.touch.total",
          "data.attributes.ac.flatFooted.total",
          "data.attributes.cmd.total",
          "data.attributes.cmd.flatFootedTotal",
        ]) {
          getSourceInfo(this.sourceInfo, k).negative.push({
            value: -8,
            name: game.i18n.localize("PF1.CondHelpless"),
          });
        }
        if (!this.flags["loseDexToAC"]) {
          for (const k of [
            "data.attributes.ac.normal.total",
            "data.attributes.ac.touch.total",
            "data.attributes.cmd.total",
          ]) {
            getSourceInfo(this.sourceInfo, k).negative.push({
              value: game.i18n.localize("PF1.ChangeFlagLoseDexToAC"),
              name: game.i18n.localize("PF1.CondHelpless"),
            });
          }
          this.flags["loseDexToAC"] = true;
        }
        break;
      case "invisible":
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: "+2",
            target: "attack",
            subTarget: "mattack",
            modifier: "untyped",
          })
        );
        getSourceInfo(this.sourceInfo, "data.attributes.attack.melee").negative.push({
          name: game.i18n.localize("PF1.CondInvisible"),
          value: +2,
        });
        break;
      case "pf1_sleep":
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -8,
            target: "defense",
            subTarget: "ac",
            modifier: "penalty",
          })
        );
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -8,
            target: "misc",
            subTarget: "cmd",
            modifier: "penalty",
          })
        );

        for (const k of [
          "data.attributes.ac.normal.total",
          "data.attributes.ac.touch.total",
          "data.attributes.ac.flatFooted.total",
          "data.attributes.cmd.total",
          "data.attributes.cmd.flatFootedTotal",
        ]) {
          getSourceInfo(this.sourceInfo, k).negative.push({
            value: -8,
            name: game.i18n.localize("PF1.CondSleep"),
          });
        }
        if (!this.flags["loseDexToAC"]) {
          for (const k of [
            "data.attributes.ac.normal.total",
            "data.attributes.ac.touch.total",
            "data.attributes.cmd.total",
          ]) {
            getSourceInfo(this.sourceInfo, k).negative.push({
              value: game.i18n.localize("PF1.ChangeFlagLoseDexToAC"),
              name: game.i18n.localize("PF1.CondSleep"),
            });
          }
          this.flags["loseDexToAC"] = true;
        }
        break;
      case "paralyzed":
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -8,
            target: "defense",
            subTarget: "ac",
            modifier: "penalty",
          })
        );
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -8,
            target: "misc",
            subTarget: "cmd",
            modifier: "penalty",
          })
        );

        for (const k of [
          "data.attributes.ac.normal.total",
          "data.attributes.ac.touch.total",
          "data.attributes.ac.flatFooted.total",
          "data.attributes.cmd.total",
          "data.attributes.cmd.flatFootedTotal",
        ]) {
          getSourceInfo(this.sourceInfo, k).negative.push({
            value: -8,
            name: game.i18n.localize("PF1.CondParalyzed"),
          });
        }
        if (!this.flags["loseDexToAC"]) {
          for (const k of [
            "data.attributes.ac.normal.total",
            "data.attributes.ac.touch.total",
            "data.attributes.cmd.total",
          ]) {
            getSourceInfo(this.sourceInfo, k).negative.push({
              value: game.i18n.localize("PF1.ChangeFlagLoseDexToAC"),
              name: game.i18n.localize("PF1.CondParalyzed"),
            });
          }
          this.flags["loseDexToAC"] = true;
        }
        break;
      case "pf1_prone":
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -4,
            target: "attack",
            subTarget: "mattack",
            modifier: "penalty",
          })
        );
        getSourceInfo(this.sourceInfo, "data.attributes.attack.melee").negative.push({
          name: game.i18n.localize("PF1.CondProne"),
          value: -4,
        });
        break;
      case "pinned":
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -8,
            target: "defense",
            subTarget: "ac",
            modifier: "penalty",
          })
        );
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -8,
            target: "misc",
            subTarget: "cmd",
            modifier: "penalty",
          })
        );

        for (const k of [
          "data.attributes.ac.normal.total",
          "data.attributes.ac.touch.total",
          "data.attributes.ac.flatFooted.total",
          "data.attributes.cmd.total",
          "data.attributes.cmd.flatFootedTotal",
        ]) {
          getSourceInfo(this.sourceInfo, k).negative.push({
            value: -8,
            name: game.i18n.localize("PF1.CondPinned"),
          });
        }
        if (!this.flags["loseDexToAC"]) {
          for (const k of [
            "data.attributes.ac.normal.total",
            "data.attributes.ac.touch.total",
            "data.attributes.cmd.total",
          ]) {
            getSourceInfo(this.sourceInfo, k).negative.push({
              value: game.i18n.localize("PF1.ChangeFlagLoseDexToAC"),
              name: game.i18n.localize("PF1.CondPinned"),
            });
          }
          this.flags["loseDexToAC"] = true;
        }
        break;
      case "cowering":
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "defense",
            subTarget: "ac",
            modifier: "penalty",
          })
        );
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "misc",
            subTarget: "cmd",
            modifier: "penalty",
          })
        );

        for (const k of [
          "data.attributes.ac.normal.total",
          "data.attributes.ac.touch.total",
          "data.attributes.ac.flatFooted.total",
          "data.attributes.cmd.total",
          "data.attributes.cmd.flatFootedTotal",
        ]) {
          getSourceInfo(this.sourceInfo, k).negative.push({
            value: -2,
            name: game.i18n.localize("PF1.CondCowering"),
          });
        }
        if (!this.flags["loseDexToAC"]) {
          for (const k of [
            "data.attributes.ac.normal.total",
            "data.attributes.ac.touch.total",
            "data.attributes.cmd.total",
          ]) {
            getSourceInfo(this.sourceInfo, k).negative.push({
              value: game.i18n.localize("PF1.ChangeFlagLoseDexToAC"),
              name: game.i18n.localize("PF1.CondCowering"),
            });
          }
          this.flags["loseDexToAC"] = true;
        }
        break;
      case "shaken":
      case "frightened":
      case "panicked":
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "attack",
            subTarget: "attack",
            modifier: "penalty",
            flavor: game.i18n.localize("PF1.CondFear"),
          })
        );
        getSourceInfo(this.sourceInfo, "data.attributes.attack.general").negative.push({
          value: -2,
          name: game.i18n.localize("PF1.CondFear"),
        });

        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "savingThrows",
            subTarget: "allSavingThrows",
            modifier: "penalty",
          })
        );
        for (const k of Object.keys(actorData.attributes.savingThrows)) {
          getSourceInfo(this.sourceInfo, `data.attributes.savingThrows.${k}.total`).negative.push({
            value: -2,
            name: game.i18n.localize("PF1.CondFear"),
          });
        }

        {
          changes.push(
            game.pf1.documentComponents.ItemChange.create({
              formula: -2,
              target: "skills",
              subTarget: "skills",
              modifier: "penalty",
            })
          );
          const flats = getChangeFlat.call(this, "skills", "penalty");
          for (const f of flats) {
            getSourceInfo(this.sourceInfo, f).negative.push({
              value: -2,
              name: game.i18n.localize("PF1.CondFear"),
            });
          }
        }

        {
          changes.push(
            game.pf1.documentComponents.ItemChange.create({
              formula: -2,
              target: "abilityChecks",
              subTarget: "allChecks",
              modifier: "penalty",
            })
          );
          const flats = getChangeFlat.call(this, "allChecks", "penalty");
          for (const f of flats) {
            getSourceInfo(this.sourceInfo, f).negative.push({
              value: -2,
              name: game.i18n.localize("PF1.CondFear"),
            });
          }
        }
        break;
      case "sickened":
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "attack",
            subTarget: "attack",
            modifier: "penalty",
            flavor: game.i18n.localize("PF1.CondSickened"),
          })
        );
        getSourceInfo(this.sourceInfo, "data.attributes.attack.general").negative.push({
          value: -2,
          name: game.i18n.localize("PF1.CondSickened"),
        });

        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "damage",
            subTarget: "mdamage",
            modifier: "penalty",
          })
        );
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "damage",
            subTarget: "rdamage",
            modifier: "penalty",
          })
        );
        getSourceInfo(this.sourceInfo, "data.attributes.damage.weapon").negative.push({
          value: -2,
          name: game.i18n.localize("PF1.CondSickened"),
        });

        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "savingThrows",
            subTarget: "allSavingThrows",
            modifier: "penalty",
          })
        );
        for (const k of Object.keys(actorData.attributes.savingThrows)) {
          getSourceInfo(this.sourceInfo, `data.attributes.savingThrows.${k}.total`).negative.push({
            value: -2,
            name: game.i18n.localize("PF1.CondSickened"),
          });
        }

        {
          changes.push(
            game.pf1.documentComponents.ItemChange.create({
              formula: -2,
              target: "skills",
              subTarget: "skills",
              modifier: "penalty",
            })
          );
          const flats = getChangeFlat.call(this, "skills", "penalty");
          for (const f of flats) {
            getSourceInfo(this.sourceInfo, f).negative.push({
              value: -2,
              name: game.i18n.localize("PF1.CondSickened"),
            });
          }
        }

        {
          changes.push(
            game.pf1.documentComponents.ItemChange.create({
              formula: -2,
              target: "abilityChecks",
              subTarget: "allChecks",
              modifier: "penalty",
            })
          );
          const flats = getChangeFlat.call(this, "allChecks", "penalty");
          for (const f of flats) {
            getSourceInfo(this.sourceInfo, f).negative.push({
              value: -2,
              name: game.i18n.localize("PF1.CondSickened"),
            });
          }
        }
        break;
      case "stunned":
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "defense",
            subTarget: "ac",
            modifier: "penalty",
          })
        );
        changes.push(
          game.pf1.documentComponents.ItemChange.create({
            formula: -2,
            target: "misc",
            subTarget: "cmd",
            modifier: "penalty",
          })
        );

        for (const k of [
          "data.attributes.ac.normal.total",
          "data.attributes.ac.touch.total",
          "data.attributes.ac.flatFooted.total",
          "data.attributes.cmd.total",
          "data.attributes.cmd.flatFootedTotal",
        ]) {
          getSourceInfo(this.sourceInfo, k).negative.push({
            value: -2,
            name: game.i18n.localize("PF1.CondStunned"),
          });
        }
        if (!this.flags["loseDexToAC"]) {
          for (const k of [
            "data.attributes.ac.normal.total",
            "data.attributes.ac.touch.total",
            "data.attributes.cmd.total",
          ]) {
            getSourceInfo(this.sourceInfo, k).negative.push({
              value: game.i18n.localize("PF1.ChangeFlagLoseDexToAC"),
              name: game.i18n.localize("PF1.CondStunned"),
            });
          }
          this.flags["loseDexToAC"] = true;
        }
        break;
    }
  }

  // Handle fatigue and exhaustion so that they don't stack
  if (actorData.attributes.conditions.exhausted) {
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: -4,
        target: "defense",
        subTarget: "ac",
        modifier: "penalty",
      })
    );
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: -4,
        target: "misc",
        subTarget: "cmd",
        modifier: "penalty",
      })
    );

    for (const k of [
      "data.attributes.ac.normal.total",
      "data.attributes.ac.touch.total",
      "data.attributes.ac.flatFooted.total",
      "data.attributes.cmd.total",
    ]) {
      getSourceInfo(this.sourceInfo, k).negative.push({
        value: -4,
        name: game.i18n.localize("PF1.CondExhausted"),
      });
    }

    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: "-4",
        target: "attack",
        subTarget: "attack",
        modifier: "penalty",
        flavor: game.i18n.localize("PF1.CondExhausted"),
      })
    );
    getSourceInfo(this.sourceInfo, "data.attributes.attack.general").negative.push({
      value: -4,
      name: game.i18n.localize("PF1.CondExhausted"),
    });
  } else if (actorData.attributes.conditions.fatigued) {
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: -2,
        target: "defense",
        subTarget: "ac",
        modifier: "penalty",
      })
    );
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: -2,
        target: "misc",
        subTarget: "cmd",
        modifier: "penalty",
      })
    );

    for (const k of [
      "data.attributes.ac.normal.total",
      "data.attributes.ac.touch.total",
      "data.attributes.ac.flatFooted.total",
      "data.attributes.cmd.total",
    ]) {
      getSourceInfo(this.sourceInfo, k).negative.push({
        value: -2,
        name: game.i18n.localize("PF1.CondFatigued"),
      });
    }

    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: "-2",
        target: "attack",
        subTarget: "attack",
        modifier: "penalty",
        flavor: game.i18n.localize("PF1.CondFatigued"),
      })
    );
    getSourceInfo(this.sourceInfo, "data.attributes.attack.general").negative.push({
      value: -2,
      name: game.i18n.localize("PF1.CondFatigued"),
    });
  }

  // Natural armor
  {
    const ac = getProperty(this.data, "data.attributes.armor.natural") || 0;
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: ac,
        target: "ac",
        subTarget: "ac",
        modifier: "natural",
      })
    );
    for (const k of ["normal", "flatFooted"]) {
      getSourceInfo(this.sourceInfo, `data.attributes.ac.${k}.total`).positive.push({
        formula: ac.toString(),
        name: game.i18n.localize("PF1.BuffTarACNatural"),
      });
    }
  }
  if (!this.flags["loseDexToAC"]) {
    // Dodge armor
    {
      const ac = getProperty(this.data, "data.attributes.armor.dodge") || 0;
      changes.push(
        game.pf1.documentComponents.ItemChange.create({
          formula: ac.toString(),
          target: "ac",
          subTarget: "ac",
          modifier: "dodge",
        })
      );
      for (const k of [
        "data.attributes.ac.normal.total",
        "data.attributes.ac.touch.total",
        "data.attributes.cmd.total",
      ]) {
        getSourceInfo(this.sourceInfo, k).positive.push({
          formula: ac.toString(),
          name: game.i18n.localize("PF1.BuffTarACDodge"),
        });
      }
    }
    // Deflection armor
    {
      const ac = getProperty(this.data, "data.attributes.armor.deflection") || 0;
      changes.push(
        game.pf1.documentComponents.ItemChange.create({
          formula: ac.toString(),
          target: "ac",
          subTarget: "ac",
          modifier: "deflection",
        })
      );
      for (const k of [
        "data.attributes.ac.normal.total",
        "data.attributes.ac.touch.total",
        "data.attributes.cmd.total",
      ]) {
        getSourceInfo(this.sourceInfo, k).positive.push({
          formula: ac.toString(),
          name: game.i18n.localize("PF1.BuffTarACDeflection"),
        });
      }
    }
  }
  // Luck armor
  {
    const ac = getProperty(this.data, "data.attributes.armor.luck") || 0;
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: ac.toString(),
        target: "ac",
        subTarget: "ac",
        modifier: "luck",
      })
    );
    for (const k of [
      "data.attributes.ac.normal.total",
      "data.attributes.ac.touch.total",
      "data.attributes.ac.flatFooted.total",
      "data.attributes.cmd.total",
    ]) {
      getSourceInfo(this.sourceInfo, k).positive.push({
        formula: ac.toString(),
        name: game.i18n.localize("PF1.BuffTarACLuck"),
      });
    }
  }
  // Other armor
  {
    const ac = getProperty(this.data, "data.attributes.armor.other") || 0;
    changes.push(
      game.pf1.documentComponents.ItemChange.create({
        formula: ac.toString(),
        target: "ac",
        subTarget: "ac",
        modifier: "untyped",
      })
    );
    for (const k of ["normal", "touch", "flatFooted"]) {
      getSourceInfo(this.sourceInfo, `data.attributes.ac.${k}.total`).positive.push({
        formula: ac.toString(),
        name: game.i18n.localize("PF1.BuffTarACGeneric"),
      });
    }
  }
};

const resetSkills = function () {
  const actorData = this.data.data;
  const skills = actorData.skills;

  for (const [sklKey, skl] of Object.entries(skills)) {
    if (!skl) continue;

    let acpPenalty = skl.acp ? actorData.attributes.acp.total : 0;
    let ablMod =
      (actorData.abilities[skl.ability]
        ? actorData.abilities[skl.ability].mod
        : actorData.aspects[skl.ability].mod) || 0;
    let specificSkillBonus = skl.changeBonus || 0;

    // Parse main skills
    let sklValue = skl.rank + (skl.rank > 0 ? 3 : 0) + skl.misc + ablMod + specificSkillBonus - acpPenalty;
    skl.mod = sklValue;

    // Parse sub-skills
    for (const [subSklKey, subSkl] of Object.entries(skl.subSkills || {})) {
      if (!subSkl) continue;
      const subSkill = skl.subSkills?.[subSklKey];
      if (!subSkill) continue;

      acpPenalty = subSkl.acp ? actorData.attributes.acp.total : 0;
      ablMod = actorData.abilities[subSkl.ability]?.mod || 0;
      specificSkillBonus = subSkl.changeBonus || 0;
      sklValue = subSkl.rank + (subSkl.cs && subSkl.rank > 0 ? 3 : 0) + ablMod + specificSkillBonus - acpPenalty;
      subSkill.mod = sklValue;
    }
  }
};

export const getSourceInfo = function (obj, key) {
  if (!obj[key]) {
    obj[key] = { negative: [], positive: [] };
  }
  return obj[key];
};

export const setSourceInfoByName = function (obj, key, name, value, positive = true) {
  const target = positive ? "positive" : "negative";
  const sourceInfo = getSourceInfo(obj, key)[target];
  const data = sourceInfo.find((o) => o.name === name);
  if (data) data.value = value;
  else {
    sourceInfo.push({
      name: name,
      value: value,
    });
  }
};

/**
 * @param {ItemChange[]} changes - An array containing all changes to check. Must be called after they received a value (by ItemChange.applyChange)
 * @param options
 * @returns {ItemChange[]} - A list of processed changes, excluding the lower-valued ones inserted (if they don't stack)
 */
export const getHighestChanges = function (changes, options = { ignoreTarget: false }) {
  const highestTemplate = {
    value: 0,
    ids: [],
    highestID: null,
  };
  const highest = Object.keys(CONFIG.PF1.bonusModifiers).reduce((cur, k) => {
    if (options.ignoreTarget) cur[k] = duplicate(highestTemplate);
    else cur[k] = {};
    return cur;
  }, {});

  for (const c of changes) {
    let h;
    if (options.ignoreTarget) h = highest[c.modifier];
    else h = highest[c.modifier][c.subTarget];

    h.ids.push(c._id);
    if (h.value < c.value || !h.highestID) {
      h.value = c.value;
      h.highestID = c._id;
    }
  }

  {
    let mod, h;
    const filterFunc = function (c) {
      if (h.highestID === c._id) return true;
      if (CONFIG.PF1.stackingBonusModifiers.indexOf(mod) === -1 && h.ids.includes(c._id)) return false;
      return true;
    };

    for (mod of Object.keys(highest)) {
      if (options.ignoreTarget) {
        h = highest[mod];
        changes = changes.filter(filterFunc);
      } else {
        for (const subTarget of Object.keys(highest[mod])) {
          h = highest[mod][subTarget];
          changes = changes.filter(filterFunc);
        }
      }
    }
  }

  return changes;
};
